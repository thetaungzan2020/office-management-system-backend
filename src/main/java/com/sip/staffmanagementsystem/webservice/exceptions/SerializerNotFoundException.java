package com.sip.staffmanagementsystem.webservice.exceptions;

public class SerializerNotFoundException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public SerializerNotFoundException(String message) {
		super(message);
	}
}
