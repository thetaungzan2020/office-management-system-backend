package com.sip.staffmanagementsystem.webservice.exceptions;

public class RequestPathNotSupportedException extends RuntimeException {

	private static final long serialVersionUID = 1L;
	
	public RequestPathNotSupportedException(String message) {
		super(message);
	}
}
