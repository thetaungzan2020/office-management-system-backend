package com.sip.staffmanagementsystem.webservice.services.impl;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.springframework.stereotype.Component;

import com.sip.staffmanagementsystem.webservice.dto.AssetProfileDto;
import com.sip.staffmanagementsystem.webservice.entities.AssetProfile;
import com.sip.staffmanagementsystem.webservice.repositories.AssetProfileRepo;
import com.sip.staffmanagementsystem.webservice.repositories.AssetTypeRepo;
import com.sip.staffmanagementsystem.webservice.services.AssetProfileService;

import lombok.Data;

@Component
@Data
public class AssetProfileServiceImpl implements AssetProfileService{
	
	private final AssetProfileRepo assetProfileRepo;
	private final AssetTypeRepo assetTypeRepo;
	
	@Override
	@Transactional
	public AssetProfileDto createAssetProfile(AssetProfileDto assetProfileDto) {
		/*
		  Optional<AssetType> assetTypeForAP =
		  assetTypeRepo.findById(assetProfile.getAssetType().getId());
		  if(assetTypeForAP.isPresent()) { AssetType foundAssetType =
		  assetTypeForAP.get(); foundAssetType.addAssetProfile(assetProfile); }
		 */
		double totalAmount = assetProfileDto.getQuantity() * assetProfileDto.getUnitPrice();
		assetProfileDto.setTotalAmount(Math.round(totalAmount));
		AssetProfile toSaveAssetProfile = AssetProfileDto.assetProfileDtoToAssetProfile(assetProfileDto);
//		toSaveAssetProfile.getAssetType().addAssetProfile(toSaveAssetProfile);
		AssetProfileDto savedAssetProfileDto =  AssetProfileDto.assetProfileToAssetProfileDto(assetProfileRepo.save(toSaveAssetProfile),false);
		return savedAssetProfileDto;
	}

	@Override
	public AssetProfileDto searchAssetProfileById(int id) {
		AssetProfile foundAssetProfile = assetProfileRepo.findById(id).get();
		return AssetProfileDto.assetProfileToAssetProfileDto(foundAssetProfile, false);
	}

	@Override
	public AssetProfileDto updateAssetProfile(AssetProfileDto assetProfileDto) {
		
		AssetProfile toUpdateAssetProfile = AssetProfileDto.assetProfileDtoToAssetProfile(assetProfileDto);
		
		AssetProfileDto updatedAssetProfileDto =  AssetProfileDto.assetProfileToAssetProfileDto(assetProfileRepo.saveAndFlush(toUpdateAssetProfile),false);
		
		return updatedAssetProfileDto;
	}

	@Override
	public List<AssetProfileDto> findAllAssetProfiles() {
		List<AssetProfileDto> foundAssetProfiles =  assetProfileRepo.findAll().stream()
													.map(ap -> AssetProfileDto.assetProfileToAssetProfileDto(ap, true))
													.collect(Collectors.toList());
		return foundAssetProfiles;
	}

	@Override
	public boolean deleteAssetProfileById(int id) {
		Optional<AssetProfile> assetProfileOptional = assetProfileRepo.findById(id);
		if(assetProfileOptional.isPresent()) {
			AssetProfile foundAssetProfile = assetProfileOptional.get();
//			AssetType assetTypeOfAP = foundAssetProfile.getAssetType();
//			assetTypeOfAP.removeAssetProfile(foundAssetProfile);
			assetProfileRepo.delete(foundAssetProfile);
			return true;
		}
		return false;
	}
	
	

}
