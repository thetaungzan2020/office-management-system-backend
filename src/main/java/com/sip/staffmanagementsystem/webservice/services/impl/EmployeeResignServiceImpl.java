package com.sip.staffmanagementsystem.webservice.services.impl;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.springframework.stereotype.Component;

import com.sip.staffmanagementsystem.webservice.dto.EmployeeDto;
import com.sip.staffmanagementsystem.webservice.dto.EmployeeResignDto;
import com.sip.staffmanagementsystem.webservice.dto.EmployeeResignFormDto;
import com.sip.staffmanagementsystem.webservice.entities.Employee;
import com.sip.staffmanagementsystem.webservice.entities.EmployeeResign;
import com.sip.staffmanagementsystem.webservice.repositories.EmployeeRepo;
import com.sip.staffmanagementsystem.webservice.repositories.EmployeeResignRepo;
import com.sip.staffmanagementsystem.webservice.services.EmployeeResignService;

import lombok.Data;

@Component
@Data
public class EmployeeResignServiceImpl implements EmployeeResignService{
	
	private final EmployeeResignRepo employeeResignRepo;
	private final EmployeeRepo employeeRepo;
	
	@Override
	@Transactional
	public EmployeeResignDto createEmployeeResign(EmployeeResignFormDto dto) {
		Map<String, Object> empAndEmpResign =  EmployeeResignFormDto.getEmployeeAndEmpResign(dto);
		Employee toUpdateEmp = (Employee) empAndEmpResign.get("employee");
		toUpdateEmp.setCreatedDate(employeeRepo.getCreatedDate(toUpdateEmp.getId()));
		toUpdateEmp.setUpdatedDate(LocalDateTime.now().toString());

		Employee updatedEmp = employeeRepo.saveAndFlush(toUpdateEmp);
		if(updatedEmp.isResign()) {

			EmployeeResign tempEmpResign = (EmployeeResign) empAndEmpResign.get("empResign");
			tempEmpResign.setEmployee(updatedEmp);
			EmployeeResign savedEmployeeResign = employeeResignRepo.save(tempEmpResign);
			return EmployeeResignDto.employeeResignToEmpResignDto(savedEmployeeResign);
		}
		EmployeeResignDto newDtoWithEmp = new EmployeeResignDto();
		newDtoWithEmp.setEmployee(EmployeeDto.employeeToEmployeeDto(updatedEmp, true));
		return newDtoWithEmp;
		
	}

	@Override
	@Transactional
	public EmployeeResignDto updateEmployeeResign(EmployeeResignFormDto dto) {
		Map<String, Object> empAndEmpResign =  EmployeeResignFormDto.getEmployeeAndEmpResign(dto);
		Employee toUpdateEmp = (Employee) empAndEmpResign.get("employee");
		Employee updatedEmp = employeeRepo.saveAndFlush(toUpdateEmp);
		EmployeeResign toUpdateEmpResign = (EmployeeResign) empAndEmpResign.get("empResign");
		toUpdateEmpResign.setEmployee(updatedEmp);
		EmployeeResign updatedEmployeeResign = employeeResignRepo.saveAndFlush(toUpdateEmpResign);
		return EmployeeResignDto.employeeResignToEmpResignDto(updatedEmployeeResign);
	}

	@Override
	public EmployeeResignDto searchEmployeeResignById(int id) {
		return EmployeeResignDto.employeeResignToEmpResignDto(employeeResignRepo.findById(id).get());
	}

	@Override
	public List<EmployeeResignDto> findAllEmployeeResign() {
		return employeeResignRepo.findAll().stream().map(empRes -> EmployeeResignDto.employeeResignToEmpResignDto(empRes)).collect(Collectors.toList());
	}

	@Override
	public void deleteEmployeeResignById(int id) {
		employeeResignRepo.deleteById(id);
	}

	
}
