package com.sip.staffmanagementsystem.webservice.services;

import java.util.List;

import org.springframework.stereotype.Service;

import com.sip.staffmanagementsystem.webservice.dto.AssetProfileDto;

@Service
public interface AssetProfileService {

	AssetProfileDto createAssetProfile(AssetProfileDto assetProfile);
	
	AssetProfileDto searchAssetProfileById(int id);
	
	AssetProfileDto updateAssetProfile(AssetProfileDto assetProfile);
	
	List<AssetProfileDto> findAllAssetProfiles();
	
	boolean deleteAssetProfileById(int id);
}
