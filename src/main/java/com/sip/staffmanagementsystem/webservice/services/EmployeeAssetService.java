package com.sip.staffmanagementsystem.webservice.services;

import java.util.List;

import org.springframework.stereotype.Service;

import com.sip.staffmanagementsystem.webservice.dto.EmployeeAssetDto;

@Service
public interface EmployeeAssetService {

	EmployeeAssetDto createEmployeeAsset(EmployeeAssetDto employeeAsset);
	EmployeeAssetDto searchEmployeeAssetById(long id);
	EmployeeAssetDto updateEmployeeAsset(EmployeeAssetDto employeeAsset);
	List<EmployeeAssetDto> findAllEmployeeAsset();
	String deleteEmployeeAsset(long id);
}
