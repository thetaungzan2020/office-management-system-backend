/*
 * package com.sip.staffmanagementsystem.webservice.services.impl;
 * 
 * import java.time.LocalDateTime; import java.util.List; import
 * java.util.Optional; import java.util.stream.Collectors;
 * 
 * 
 * import org.springframework.stereotype.Component;
 * 
 * import com.sip.staffmanagementsystem.webservice.dto.AssetTypeDto; import
 * com.sip.staffmanagementsystem.webservice.entities.AssetType; import
 * com.sip.staffmanagementsystem.webservice.repositories.AssetProfileRepo;
 * import com.sip.staffmanagementsystem.webservice.repositories.AssetTypeRepo;
 * import com.sip.staffmanagementsystem.webservice.services.AssetTypeService;
 * 
 * import lombok.Data;
 * 
 * @Component
 * 
 * @Data public class AssetTypeServiceImpl implements AssetTypeService{
 * 
 * private final AssetTypeRepo assetTypeRepo; private final AssetProfileRepo
 * assetProfileRepo;
 * 
 * @Override public AssetTypeDto createAssetType(AssetTypeDto assetTypeDto) {
 * assetTypeDto.setStartedDate(LocalDateTime.now()); AssetType toSaveAssetType =
 * AssetTypeDto.assetTypeDtoToAssetType(assetTypeDto); AssetType savedAssetType
 * = assetTypeRepo.save(toSaveAssetType);
 * 
 * return AssetTypeDto.assetTypeToAssetTypeDto(savedAssetType, false); }
 * 
 * @Override public AssetTypeDto searchAssetTypeById(int id) { AssetType
 * foundAssetType = assetTypeRepo.findById(id).get();
 * 
 * return AssetTypeDto.assetTypeToAssetTypeDto(foundAssetType, false); }
 * 
 * @Override public AssetTypeDto updateAsset(AssetTypeDto assetTypeDto) {
 * 
 * LocalDateTime updatedDate = LocalDateTime.now();
 * assetTypeDto.setUpdatedDate(updatedDate); AssetType toUpdateAssetType =
 * AssetTypeDto.assetTypeDtoToAssetType(assetTypeDto); AssetType
 * updatedAssetType = assetTypeRepo.saveAndFlush(toUpdateAssetType);
 * 
 * return AssetTypeDto.assetTypeToAssetTypeDto(updatedAssetType, false);
 * 
 * }
 * 
 * @Override public List<AssetTypeDto> findAllAssetTypes() {
 * 
 * List<AssetTypeDto> allAssetTypesDto = assetTypeRepo.findAll().stream()
 * .map(at -> AssetTypeDto.assetTypeToAssetTypeDto(at, true))
 * .collect(Collectors.toList());
 * 
 * return allAssetTypesDto; }
 * 
 * @Override public String deleteAssetTypeById(int id) { Optional<AssetType>
 * foundAssetType = assetTypeRepo.findById(id); if (!foundAssetType.isPresent())
 * return "AssetType not found"; if (foundAssetType.isPresent()) { if
 * (foundAssetType.get().getAssetProfiles().size() < 1) {
 * assetTypeRepo.delete(foundAssetType.get()); return "Successfully deleted"; }
 * } return "Can't Delete AssetType : "+id; }
 * 
 * }
 * 
 * 
 */
package com.sip.staffmanagementsystem.webservice.services.impl;


public class AssetTypeServiceImpl {
    
    public static void main(String[] args) {
        String str2 = "01011111";
        
        String ans2 = new AssetTypeServiceImpl().maximumOddBinaryNumber(str2);
        
        System.out.println("Answer for " + str2 + ": " + ans2); // Output: 1001
    }
    
    public String maximumOddBinaryNumber(String s) {
        int cnt1 = 0, cnt0 = 0;
        for (char a : s.toCharArray()) {
            if (a == '1') cnt1++;
            else if (a == '0') cnt0++;
        }
        String ans = "";
        for (int i = 0; i < cnt1 - 1; i++) {
            ans = ans+"1";
        }
        for (int i = 0; i < cnt0; i++) {
            ans = ans+"0";
        }
        ans = ans+"1";
        return ans;
    }
}


