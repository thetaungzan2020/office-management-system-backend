package com.sip.staffmanagementsystem.webservice.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.sip.staffmanagementsystem.webservice.entities.Designation;
import com.sip.staffmanagementsystem.webservice.repositories.DesignationRepo;
import com.sip.staffmanagementsystem.webservice.services.DesignationService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class DesignationServiceImpl implements DesignationService {

	@Autowired
	private DesignationRepo designationRepo;

	@Override
	public Designation createDesignation(Designation designation) {
		Designation save = designationRepo.save(designation);
		return save;
	}
	
	@Override
	public List<Designation> searchAllByDesignation() {
		return designationRepo.findAll();
	}

	@Override
	public Designation searchByDesignationId(Integer id) {

		return designationRepo.findById(id).orElse(null);
	}

	@Override
	public void deleteByDesignationId(Integer id) {
		boolean existsById = designationRepo.existsById(id);
		if (!existsById) {
			log.info("This Designation Id is existed.");
		} else {
			designationRepo.deleteById(id);
		}
	}

	@Override
	public void deleteAllDesignation() {
		designationRepo.deleteAll();
	}

	
}
