package com.sip.staffmanagementsystem.webservice.services;

import java.util.List;

import org.springframework.stereotype.Service;

import com.sip.staffmanagementsystem.webservice.dto.EmployeeResignDto;
import com.sip.staffmanagementsystem.webservice.dto.EmployeeResignFormDto;

@Service
public interface EmployeeResignService {
	
	public EmployeeResignDto createEmployeeResign(EmployeeResignFormDto dto);
	public EmployeeResignDto updateEmployeeResign(EmployeeResignFormDto dto);
	public EmployeeResignDto searchEmployeeResignById(int id);
	public List<EmployeeResignDto> findAllEmployeeResign();
	public void deleteEmployeeResignById(int id);

}
