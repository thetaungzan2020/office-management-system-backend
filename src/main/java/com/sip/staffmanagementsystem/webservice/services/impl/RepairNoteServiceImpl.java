package com.sip.staffmanagementsystem.webservice.services.impl;

import java.util.List;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.springframework.stereotype.Component;

import com.sip.staffmanagementsystem.webservice.dto.RepairNoteDto;
import com.sip.staffmanagementsystem.webservice.entities.AssetProfile;
import com.sip.staffmanagementsystem.webservice.entities.Employee;
import com.sip.staffmanagementsystem.webservice.entities.RepairNote;
import com.sip.staffmanagementsystem.webservice.repositories.RepairNoteRepo;
import com.sip.staffmanagementsystem.webservice.services.RepairNoteService;

import lombok.RequiredArgsConstructor;

@Component
@RequiredArgsConstructor
public class RepairNoteServiceImpl implements RepairNoteService{

	private final RepairNoteRepo noteRepo;

	@Override
	@Transactional
	public RepairNoteDto createRepairNote(RepairNoteDto repairNoteDto) {
		RepairNote toSaveRepairNote = RepairNoteDto.repairNoteDtoToRepairNote(repairNoteDto);
		
		Employee employeeOfRepairNote = toSaveRepairNote.getEmployee();
		employeeOfRepairNote.addRepairNote(toSaveRepairNote);
		
		AssetProfile assetProfile = toSaveRepairNote.getAssetProfile();
		assetProfile.addRepairNote(toSaveRepairNote);
	
		RepairNoteDto savedRepairNoteDto = RepairNoteDto.repairNoteToRepairNoteDto(noteRepo.save(toSaveRepairNote), false , false);
		return savedRepairNoteDto;
	}

	@Override
	public RepairNoteDto updateRepairNote(RepairNoteDto repairNoteDto) {
		
		RepairNote toUpdateRepairNote = RepairNoteDto.repairNoteDtoToRepairNote(repairNoteDto);
		RepairNoteDto updatedRepairNoteDto =  RepairNoteDto.repairNoteToRepairNoteDto(noteRepo.saveAndFlush(toUpdateRepairNote), false, false);
		return updatedRepairNoteDto;
	}

	@Override
	public RepairNoteDto searchRepairNote(int id) {
		
		RepairNoteDto foundRepairNoteDto = RepairNoteDto.repairNoteToRepairNoteDto(noteRepo.findById(id).get(), false, false);
		
		return foundRepairNoteDto;
	}

	@Override
	public List<RepairNoteDto> findAllRepairNote() {
		
		List<RepairNoteDto> allRepairNoteDto = noteRepo.findAll().stream()
												.map(rn -> RepairNoteDto.repairNoteToRepairNoteDto(rn, false ,false))
												.collect(Collectors.toList());
		
		return allRepairNoteDto;
	}

	@Override
	@Transactional
	public void deleteRepairNote(int id) {
		
		RepairNote foundRepairNote = noteRepo.findById(id).get();
		
		Employee employeeOfRepairNote = foundRepairNote.getEmployee();
		employeeOfRepairNote.removeRepairNote(foundRepairNote);
		
		AssetProfile assetProfileOfRepairNote = foundRepairNote.getAssetProfile();
		assetProfileOfRepairNote.removeRepairNote(foundRepairNote);
		
		noteRepo.delete(foundRepairNote);
		
	}
	
	
}
