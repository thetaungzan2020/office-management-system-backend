package com.sip.staffmanagementsystem.webservice.services;

import java.util.List;

import org.springframework.stereotype.Service;

import com.sip.staffmanagementsystem.webservice.entities.Designation;

@Service
public interface DesignationService {

	Designation createDesignation(Designation designation);
	
	List<Designation> searchAllByDesignation();

	Designation searchByDesignationId(Integer id);

	void deleteByDesignationId(Integer id);

	void deleteAllDesignation();
}
