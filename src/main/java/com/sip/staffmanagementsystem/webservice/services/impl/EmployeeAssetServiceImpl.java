package com.sip.staffmanagementsystem.webservice.services.impl;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.springframework.stereotype.Component;

import com.sip.staffmanagementsystem.webservice.dto.EmployeeAssetDto;
import com.sip.staffmanagementsystem.webservice.entities.AssetProfile;
import com.sip.staffmanagementsystem.webservice.entities.Employee;
import com.sip.staffmanagementsystem.webservice.entities.EmployeeAsset;
import com.sip.staffmanagementsystem.webservice.repositories.EmployeeAssetProfileRepo;
import com.sip.staffmanagementsystem.webservice.repositories.EmployeeRepo;
import com.sip.staffmanagementsystem.webservice.services.EmployeeAssetService;

import lombok.Data;

@Component
@Data
public class EmployeeAssetServiceImpl implements EmployeeAssetService{
	
	private final EmployeeAssetProfileRepo employeeAssetRepo;
	private final EmployeeRepo employeeRepo;
	
	@Override
	@Transactional
	public EmployeeAssetDto createEmployeeAsset(EmployeeAssetDto employeeAssetDto) {
		
		EmployeeAsset toSaveEmployee = EmployeeAssetDto.employeeAssetDtoToEmployeeAsset(employeeAssetDto);
		
		Employee employeeOfEmpAsset = toSaveEmployee.getEmployee();		
		employeeOfEmpAsset.addEmployeeAsset(toSaveEmployee);
		
		AssetProfile assetProfileOfEmpAsset = toSaveEmployee.getAssetProfile();
		assetProfileOfEmpAsset.addEmployeeAsset(toSaveEmployee);
		
		EmployeeAsset savedEmployeeAsset = employeeAssetRepo.save(toSaveEmployee);
		
		return EmployeeAssetDto.employeeAssetToEmployeeAssetDto(savedEmployeeAsset,false, false);
	}

	@Override
	public EmployeeAssetDto searchEmployeeAssetById(long id) {
		
		EmployeeAssetDto foundEmployeeAssetDto = EmployeeAssetDto.employeeAssetToEmployeeAssetDto(employeeAssetRepo.findById(id).get(), false , false);
		
		return foundEmployeeAssetDto;
	}

	@Override
	public EmployeeAssetDto updateEmployeeAsset(EmployeeAssetDto employeeAssetDto) {
		
		EmployeeAsset toUpdateEmployeeAsset = EmployeeAssetDto.employeeAssetDtoToEmployeeAsset(employeeAssetDto);
		
		return EmployeeAssetDto.employeeAssetToEmployeeAssetDto(employeeAssetRepo.saveAndFlush(toUpdateEmployeeAsset), false , false);
	}

	@Override
	public List<EmployeeAssetDto> findAllEmployeeAsset() {
		
		List<EmployeeAssetDto> allEmployeeAsset =  employeeAssetRepo.findAll().stream()
													.map(ea -> EmployeeAssetDto.employeeAssetToEmployeeAssetDto(ea, false, false))
													.collect(Collectors.toList());
		
		return allEmployeeAsset;
	}

	@Override
	public String deleteEmployeeAsset(long id) {
		Optional<EmployeeAsset> foundEmpAssetOptional = employeeAssetRepo.findById(id);
		
		if(foundEmpAssetOptional.isPresent()) {
			EmployeeAsset foundEmployeeAsset = foundEmpAssetOptional.get();
			Employee employeeOfEmpAsset = foundEmployeeAsset.getEmployee();
			employeeOfEmpAsset.removeEmployeeAsset(foundEmployeeAsset);
			
			employeeAssetRepo.delete(foundEmployeeAsset);
			return "Successfully Deleted";
		}
		
		return "Fail To Delete";
	}

}
