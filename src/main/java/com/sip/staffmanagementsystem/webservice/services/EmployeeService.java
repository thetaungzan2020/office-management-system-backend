package com.sip.staffmanagementsystem.webservice.services;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.sip.staffmanagementsystem.webservice.dto.EmployeeDto;
import com.sip.staffmanagementsystem.webservice.dto.EmployeeFormDto;

@Service
public interface EmployeeService {
	EmployeeDto createEmployee(EmployeeFormDto employeeDto);
	
	EmployeeDto searchEmployeeById(int id);
	
	EmployeeDto updateEmployee(EmployeeFormDto employeeDto);
	
	List<EmployeeDto> findAllEmployees();
	
	boolean deleteEmployeeById(int id);
	
	Map<String, Object> searchPhotoByEmpId(int id);
	
	Map<String, Integer> getCounts();
	
}
