package com.sip.staffmanagementsystem.webservice.services;

import java.util.List;

import org.springframework.stereotype.Service;

import com.sip.staffmanagementsystem.webservice.dto.RepairNoteDto;

@Service
public interface RepairNoteService {

	public RepairNoteDto createRepairNote(RepairNoteDto repairNoteDto);
	
	public RepairNoteDto updateRepairNote(RepairNoteDto repairNoteDto);
	
	public RepairNoteDto searchRepairNote(int id);
	
	public List<RepairNoteDto> findAllRepairNote();
	
	public void deleteRepairNote(int id);
}
