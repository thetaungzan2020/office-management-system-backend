package com.sip.staffmanagementsystem.webservice.services.impl;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.stereotype.Component;

import com.sip.staffmanagementsystem.webservice.dto.EmployeeDto;
import com.sip.staffmanagementsystem.webservice.dto.EmployeeFormDto;
import com.sip.staffmanagementsystem.webservice.entities.Employee;
import com.sip.staffmanagementsystem.webservice.repositories.EmployeeRepo;
import com.sip.staffmanagementsystem.webservice.services.EmployeeService;

import lombok.Data;

@Component
@Data
public class EmployeeServiceImpl implements EmployeeService{
	
	private final EmployeeRepo employeeRepo;
	
	@Override
	public EmployeeDto createEmployee(EmployeeFormDto employeeDto) {
		
		Employee toSaveEmployee = EmployeeFormDto.employeeFormDtoToEmployee(employeeDto);
		toSaveEmployee.setCreatedDate(LocalDateTime.now().toString());
		EmployeeDto savedEmployee = EmployeeDto.employeeToEmployeeDto( employeeRepo.save(toSaveEmployee), false);
		
		return savedEmployee;
	}
	
	@Override
	public List<EmployeeDto> findAllEmployees(){
		
		List<EmployeeDto> allEmployeeDto =  employeeRepo.findAllWithoutProfile();
		return allEmployeeDto;
	}
	
	@Override
	public Map<String, Integer> getCounts() {
		Map<String, Integer> empAndCounts = new HashMap<String, Integer>();
		empAndCounts.put("currentEmp", employeeRepo.getCurrentEmployeeCount());
		empAndCounts.put("resignEmp", employeeRepo.getResignEmployeeCount());
		return empAndCounts;
	}
	
	@Override
	public EmployeeDto searchEmployeeById(int id) {
		
		EmployeeDto foundEmployeeDto = EmployeeDto.employeeToEmployeeDto(employeeRepo.findById(id).get(), false);
		
		return foundEmployeeDto;
	}
	
	
	@Override
	public EmployeeDto updateEmployee(EmployeeFormDto employeeDto) {
		
		Employee toUpdateEmployee = EmployeeFormDto.employeeFormDtoToEmployee(employeeDto);
		
		EmployeeDto updatedEmployeeDto = EmployeeDto.employeeToEmployeeDto(employeeRepo.saveAndFlush(toUpdateEmployee), false);
		
		return updatedEmployeeDto;
		
	}
	
	@Override
	public boolean deleteEmployeeById(int id) {
		Optional<Employee> foundEmployee = employeeRepo.findById(id);
		
		if (foundEmployee.isPresent()) {
			employeeRepo.delete(foundEmployee.get());
			
			return true;
		}
		
		return false;
	}

	@Override
	public Map<String, Object> searchPhotoByEmpId(int id) {
		String profileData = employeeRepo.findEmpPhotoWithId(id);
		Map<String, Object> toResponse = new HashMap<>();
		toResponse.put("profile", profileData);
		toResponse.put("id", id);
		return toResponse;
	}

}
