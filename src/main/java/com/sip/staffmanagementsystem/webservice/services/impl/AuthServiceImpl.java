/*
package com.sip.staffmanagementsystem.webservice.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.sip.staffmanagementsystem.webservice.entities.User;
import com.sip.staffmanagementsystem.webservice.repositories.AuthRepo;
import com.sip.staffmanagementsystem.webservice.services.AuthService;

@Component
public class AuthServiceImpl implements AuthService {

	@Autowired
	private AuthRepo authRepo;

	@Override
	public User createAuth(User auth) {
		User saveAuth = authRepo.save(auth);
		return saveAuth;
	}

	@Override
	public List<User> searchAllByAuth() {
		List<User> auths = authRepo.findAll();
		return auths;
	}

	@Override
	public User searchByAuthId(Integer id) {
		return authRepo.findById(id).orElse(null);
	}

	@Override
	public void deleteByAuthId(Integer id) {
		boolean exists = authRepo.existsById(id);

		if (exists) {
			authRepo.deleteById(id);
		}
	}

	@Override
	public void deleteAllAuth() {
		authRepo.deleteAll();
	}

}
*/
