/*
 * package com.sip.staffmanagementsystem.webservice.services;
 * 
 * import java.util.List;
 * 
 * import org.springframework.stereotype.Service;
 * 
 * import com.sip.staffmanagementsystem.webservice.dto.AssetTypeDto;
 * 
 * @Service public interface AssetTypeService {
 * 
 * AssetTypeDto createAssetType(AssetTypeDto assetType);
 * 
 * AssetTypeDto searchAssetTypeById(int id);
 * 
 * AssetTypeDto updateAsset(AssetTypeDto assetType);
 * 
 * List<AssetTypeDto> findAllAssetTypes();
 * 
 * String deleteAssetTypeById(int id); }
 */