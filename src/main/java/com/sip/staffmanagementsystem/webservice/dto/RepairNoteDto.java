package com.sip.staffmanagementsystem.webservice.dto;

import java.util.Date;

import com.sip.staffmanagementsystem.webservice.entities.AssetProfile;
import com.sip.staffmanagementsystem.webservice.entities.Employee;
import com.sip.staffmanagementsystem.webservice.entities.RepairNote;

import lombok.Data;

@Data
public class RepairNoteDto {
	private int id;
	private Date repairedDate;
	private int cost;
	private String remark;
	private AssetProfileDto assetProfile;
	private EmployeeDto employee;
	
	public static RepairNoteDto repairNoteToRepairNoteDto(RepairNote note, boolean forEmployee, boolean forAssetProfile) {
		RepairNoteDto dto = new RepairNoteDto();
		dto.setId(note.getId());
		dto.setRepairedDate(note.getRepairedDate());
		dto.setCost(note.getCost());
		dto.setRemark(note.getRemark());
		
		if (!forAssetProfile) {
//		dto.setAssetProfile(note.getAssetProfile());
			AssetProfileDto assetProfileDto = AssetProfileDto.assetProfileToAssetProfileDto(note.getAssetProfile(), true); 
			dto.setAssetProfile(assetProfileDto);
		}
		
		if (!forEmployee) {
//		dto.setEmployee(note.getEmployee());
			EmployeeDto employeeDto = EmployeeDto.employeeToEmployeeDto(note.getEmployee(), true);
			dto.setEmployee(employeeDto);
		}
		
		return dto;
	}
	
	public static RepairNote repairNoteDtoToRepairNote(RepairNoteDto dto) {
		RepairNote note = new RepairNote();
		note.setId(dto.getId());
		note.setRepairedDate(dto.getRepairedDate());
		note.setCost(dto.getCost());
		note.setRemark(dto.getRemark());
//		note.setAssetProfile(dto.getAssetProfile());
		AssetProfile assetProfileFromDto = AssetProfileDto.assetProfileDtoToAssetProfile(dto.getAssetProfile());
		note.setAssetProfile(assetProfileFromDto);
//		note.setEmployee(dto.getEmployee());
		Employee employeeFromDto = EmployeeDto.employeeDtoToEmployee(dto.getEmployee());
		note.setEmployee(employeeFromDto);
		return note;
	}
}
