package com.sip.staffmanagementsystem.webservice.dto;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import com.sip.staffmanagementsystem.webservice.entities.Designation;
import com.sip.staffmanagementsystem.webservice.entities.Employee;
import com.sip.staffmanagementsystem.webservice.entities.EmployeeResign;
import com.sip.staffmanagementsystem.webservice.entities.type.GenderType;

import lombok.Data;

@Data
public class EmployeeResignFormDto {
	private int id;
	private String employeeCode;
	private GenderType genderType;
	private String name;
	private Designation designation;
	private String idNumber;
	private Date dateOfBirth;
	private Date actualDateOfBirth;
	private Date jointDate;
	private Date contractStartDate;
	private Date contractEndDate;
	private boolean isResign;
	private String contactNo;
	private String bankAccount;
	private String address;
	private String emergencyContactNo;
	private String email;
	private String profile;
	private String profileName;
	private Date resignDate;
	private String remark;
	
	public static Map<String, Object> getEmployeeAndEmpResign(EmployeeResignFormDto dto){
		Map<String, Object> toReturn = new HashMap<>();
		Employee employee = new Employee();
		employee.setId(dto.getId());
		employee.setEmployeeCode(dto.getEmployeeCode());
		employee.setGenderType(dto.getGenderType());
		employee.setName(dto.getName());
		employee.setDesignation(dto.getDesignation());
		employee.setIdNumber(dto.getIdNumber());
		employee.setDateOfBirth(dto.getDateOfBirth());
		employee.setActualDateOfBirth(dto.getActualDateOfBirth());
		employee.setJointDate(dto.getJointDate());
		employee.setContractStartDate(dto.getContractStartDate());
		employee.setContractEndDate(dto.getContractEndDate());
		employee.setResign(dto.isResign());
		employee.setContactNo(dto.getContactNo());
		employee.setBankAccount(dto.getBankAccount());
		employee.setAddress(dto.getAddress());
		employee.setEmergencyContactNo(dto.getEmergencyContactNo());
		employee.setEmail(dto.getEmail());
		employee.setProfileName(dto.getProfileName());
		employee.setProfile(dto.getProfile());
		EmployeeResign empResign = new EmployeeResign();
		empResign.setResignDate(dto.getResignDate());
		empResign.setRemark(dto.getRemark());
		toReturn.put("employee", employee);
		toReturn.put("empResign", empResign);
		return toReturn;
	}
}
