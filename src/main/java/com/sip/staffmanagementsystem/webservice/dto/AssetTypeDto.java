package com.sip.staffmanagementsystem.webservice.dto;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import com.sip.staffmanagementsystem.webservice.entities.AssetProfile;
import com.sip.staffmanagementsystem.webservice.entities.AssetType;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@AllArgsConstructor
@RequiredArgsConstructor
public class AssetTypeDto {
	private int id;
	private String name;
	private String description;

	private LocalDateTime startedDate;
	private LocalDateTime updatedDate;
	private boolean enable;

	private List<AssetProfileDto> assetProfiles = new ArrayList<AssetProfileDto>();

	public static AssetTypeDto assetTypeToAssetTypeDto(AssetType assetType, boolean forAll) {

		AssetTypeDto dto = new AssetTypeDto();
		dto.setId(assetType.getId());
		dto.setName(assetType.getName());
		dto.setDescription(assetType.getDescription());
		dto.setStartedDate(assetType.getStartedDate());
		dto.setUpdatedDate(assetType.getUpdatedDate());
		dto.setEnable(assetType.isEnable());
		if (!forAll) {
//			dto.setAssetProfiles(assetType.getAssetProfiles());
			List<AssetProfileDto> assetProfileDtoList = assetType.getAssetProfiles().stream()
					.map(ap -> AssetProfileDto.assetProfileToAssetProfileDto(ap, false)).collect(Collectors.toList());

			dto.setAssetProfiles(assetProfileDtoList);
		}

		return dto;
	}

	public static AssetType assetTypeDtoToAssetType(AssetTypeDto dto) {

		AssetType assetType = new AssetType();
		assetType.setId(dto.getId());
		assetType.setName(dto.getName());
		assetType.setDescription(dto.getDescription());
		assetType.setStartedDate(dto.getStartedDate());
		assetType.setUpdatedDate(dto.getUpdatedDate());
		assetType.setEnable(dto.isEnable());
//		assetType.setAssetProfiles(dto.getAssetProfiles());
		if (dto.getAssetProfiles() != null) {

			List<AssetProfile> assetProfilesFromDto = dto.getAssetProfiles().stream()
					.map(ap -> AssetProfileDto.assetProfileDtoToAssetProfile(ap)).collect(Collectors.toList());
			assetType.setAssetProfiles(assetProfilesFromDto);
		}

		return assetType;
	}

}
