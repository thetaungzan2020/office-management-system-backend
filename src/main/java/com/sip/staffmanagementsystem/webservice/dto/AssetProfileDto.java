package com.sip.staffmanagementsystem.webservice.dto;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import com.sip.staffmanagementsystem.webservice.entities.AssetProfile;
import com.sip.staffmanagementsystem.webservice.entities.EmployeeAsset;
import com.sip.staffmanagementsystem.webservice.entities.RepairNote;
import com.sip.staffmanagementsystem.webservice.entities.type.AssetType;

import lombok.Data;

@Data
public class AssetProfileDto {

	private int id;
	private String assetName;
	private String serialNumber;
	private String computerModel;
	private String originalHardDiskSize;
	private String newHardDiskSize;
	private String memorySize;
	private String ramType;
	private String ramSpeed;
	private String osVersion;
	private String officeName;
	private String officeVersion;
	private String officeStatus;
	private String antivirusVersion;
	private String antivirusStatus;
	private boolean dvdDriveInclude;
	private String cpu;
	private Date purchaseDate;
	private float unitPrice;
	private int quantity;
	private double totalAmount;
	private String poNos;
	private String supplier;
	private String supplierPhoneNumber;
	private String assignTo;
	private String coveragePeriod;
	private String remarks;
	private boolean isReturnToSip;
	private String returnRemark;
	private boolean isRemoved;
	private String removedRemark;
	private Date removedDate;
	private AssetType assetType;

	private List<RepairNoteDto> repairNotes = new ArrayList<RepairNoteDto>();
	
	
	private List<EmployeeAssetDto> employeeAssets = new ArrayList<EmployeeAssetDto>();
	
	public static AssetProfileDto assetProfileToAssetProfileDto(AssetProfile assetProfile, boolean forAll) {
		AssetProfileDto dto = new AssetProfileDto();
		dto.setId(assetProfile.getId());
		dto.setAssetName(assetProfile.getAssetName());
		dto.setSerialNumber(assetProfile.getSerialNumber());
		dto.setComputerModel(assetProfile.getComputerModel());
		dto.setOriginalHardDiskSize(assetProfile.getOriginalHardDiskSize());
		dto.setNewHardDiskSize(assetProfile.getNewHardDiskSize());
		dto.setMemorySize(assetProfile.getMemorySize());
		dto.setRamType(assetProfile.getRamType());
		dto.setRamSpeed(assetProfile.getRamSpeed());
		dto.setOsVersion(assetProfile.getOsVersion());
		dto.setOfficeName(assetProfile.getOfficeName());
		dto.setOfficeVersion(assetProfile.getOfficeVersion());
		dto.setOfficeStatus(assetProfile.getOfficeStatus());
		dto.setDvdDriveInclude(assetProfile.isDvdDriveInclude());
		dto.setCpu(assetProfile.getCpu());
		dto.setPurchaseDate(assetProfile.getPurchaseDate());
		dto.setUnitPrice(assetProfile.getUnitPrice());
		dto.setQuantity(assetProfile.getQuantity());
		dto.setTotalAmount(assetProfile.getTotalAmount());
		dto.setPoNos(assetProfile.getPoNos());
		dto.setSupplier(assetProfile.getSupplier());
		dto.setSupplierPhoneNumber(assetProfile.getSupplierPhoneNumber());
		dto.setAssignTo(assetProfile.getAssignTo());
		dto.setCoveragePeriod(assetProfile.getCoveragePeriod());
		dto.setRemarks(assetProfile.getRemarks());
		dto.setReturnToSip(assetProfile.isReturnToSip());
		dto.setReturnRemark(assetProfile.getReturnRemark());
		dto.setRemoved(assetProfile.isRemoved());
		dto.setRemovedRemark(assetProfile.getRemovedRemark());
		dto.setRemovedDate(assetProfile.getRemovedDate());
		
		if (!forAll) {
//			dto.setRepairNotes(assetProfile.getRepairNotes());
			List<RepairNoteDto> repairNoteDtos = assetProfile.getRepairNotes().stream()
												 .map(rn -> RepairNoteDto.repairNoteToRepairNoteDto(rn, false, true))
												 .collect(Collectors.toList());
			dto.setRepairNotes(repairNoteDtos);
			
//			dto.setEmployeeAssets(assetProfile.getEmployeeAssets());
			List<EmployeeAssetDto> assetDtos = assetProfile.getEmployeeAssets().stream()
											   .map(ea -> EmployeeAssetDto.employeeAssetToEmployeeAssetDto(ea, false, true))
											   .collect(Collectors.toList());
			dto.setEmployeeAssets(assetDtos);
		}
		
		dto.setAssetType(assetProfile.getAssetType());
		return dto;
	}
	
	public static AssetProfile assetProfileDtoToAssetProfile(AssetProfileDto dto) {
		AssetProfile assetProfile = new AssetProfile();
		assetProfile.setId(dto.getId());
		assetProfile.setAssetName(dto.getAssetName());
		assetProfile.setSerialNumber(dto.getSerialNumber());
		assetProfile.setComputerModel(dto.getComputerModel());
		assetProfile.setOriginalHardDiskSize(dto.getOriginalHardDiskSize());
		assetProfile.setNewHardDiskSize(dto.getNewHardDiskSize());
		assetProfile.setMemorySize(dto.getMemorySize());
		assetProfile.setRamType(dto.getRamType());
		assetProfile.setRamSpeed(dto.getRamSpeed());
		assetProfile.setOsVersion(dto.getOsVersion());
		assetProfile.setOfficeName(dto.getOfficeName());
		assetProfile.setOfficeVersion(dto.getOfficeVersion());
		assetProfile.setOfficeStatus(dto.getOfficeStatus());
		assetProfile.setDvdDriveInclude(dto.isDvdDriveInclude());
		assetProfile.setCpu(dto.getCpu());
		assetProfile.setPurchaseDate(dto.getPurchaseDate());
		assetProfile.setUnitPrice(dto.getUnitPrice());
		assetProfile.setQuantity(dto.getQuantity());
		assetProfile.setTotalAmount(dto.getTotalAmount());
		assetProfile.setPoNos(dto.getPoNos());
		assetProfile.setSupplier(dto.getSupplier());
		assetProfile.setSupplierPhoneNumber(dto.getSupplierPhoneNumber());
		assetProfile.setAssignTo(dto.getAssignTo());
		assetProfile.setCoveragePeriod(dto.getCoveragePeriod());
		assetProfile.setRemarks(dto.getRemarks());
		assetProfile.setReturnToSip(dto.isReturnToSip());
		assetProfile.setReturnRemark(dto.getReturnRemark());
		assetProfile.setRemoved(dto.isRemoved());
		assetProfile.setRemovedRemark(dto.getRemovedRemark());
		assetProfile.setRemovedDate(dto.getRemovedDate());
		assetProfile.setAssetType(dto.getAssetType());
//		assetProfile.setRepairNotes(dto.getRepairNotes());
		List<RepairNote> repairNotesFromDto = dto.getRepairNotes().stream()
									   .map(rn -> RepairNoteDto.repairNoteDtoToRepairNote(rn))
									   .collect(Collectors.toList());
		
		assetProfile.setRepairNotes(repairNotesFromDto);
		
//		assetProfile.setEmployeeAssets(dto.getEmployeeAssetDtos());
		List<EmployeeAsset> employeeAssetsFromDto = dto.getEmployeeAssets().stream()
											 .map(ea -> EmployeeAssetDto.employeeAssetDtoToEmployeeAsset(ea))
											 .collect(Collectors.toList());
		assetProfile.setEmployeeAssets(employeeAssetsFromDto);
		return assetProfile;
	}
}
