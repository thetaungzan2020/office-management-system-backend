package com.sip.staffmanagementsystem.webservice.dto;

import java.util.Date;

import com.sip.staffmanagementsystem.webservice.entities.AssetProfile;
import com.sip.staffmanagementsystem.webservice.entities.Employee;
import com.sip.staffmanagementsystem.webservice.entities.EmployeeAsset;

import lombok.Data;

@Data
public class EmployeeAssetDto {
	
	private Long id;
	private Date startDate;
	private Date endDate;
	private boolean isCurrentUser;
	private String remarks;
	
	private EmployeeDto employee;
	
	private AssetProfileDto assetProfile;
	
	public static EmployeeAssetDto employeeAssetToEmployeeAssetDto(EmployeeAsset asset, boolean forEmployee, boolean forAssetProfile) {
		EmployeeAssetDto dto = new EmployeeAssetDto();
		
		dto.setId(asset.getId());
		dto.setStartDate(asset.getStartDate());
		dto.setEndDate(asset.getEndDate());
		dto.setCurrentUser(asset.isCurrentUser());
		dto.setRemarks(asset.getRemarks());
		if (!forEmployee) {
//			dto.setEmployee(asset.getEmployee());
			EmployeeDto employeeDto = EmployeeDto.employeeToEmployeeDto(asset.getEmployee(), true);
			dto.setEmployee(employeeDto);
		}
		if (!forAssetProfile) {
//		dto.setAssetProfile(asset.getAssetProfile());
			AssetProfileDto assetProfileDto = AssetProfileDto.assetProfileToAssetProfileDto(asset.getAssetProfile(), true);
			dto.setAssetProfile(assetProfileDto);
		}
		return dto;
	}
	
	public static EmployeeAsset employeeAssetDtoToEmployeeAsset(EmployeeAssetDto dto) {
		EmployeeAsset asset = new EmployeeAsset();
		
		asset.setId(dto.getId());
		asset.setStartDate(dto.getStartDate());
		asset.setEndDate(dto.getEndDate());
		asset.setCurrentUser(dto.isCurrentUser());
		asset.setRemarks(dto.getRemarks());
		Employee employeeFromDto = EmployeeDto.employeeDtoToEmployee(dto.getEmployee());
		asset.setEmployee(employeeFromDto);
//		asset.setEmployee(dto.getEmployee());
		
		AssetProfile assetProfileFromDto = AssetProfileDto.assetProfileDtoToAssetProfile(dto.getAssetProfile());
		asset.setAssetProfile(assetProfileFromDto);
//		asset.setAssetProfile(dto.getAssetProfile());
		
		return asset;
	}
}
