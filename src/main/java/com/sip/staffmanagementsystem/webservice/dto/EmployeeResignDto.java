package com.sip.staffmanagementsystem.webservice.dto;

import java.util.Date;

import com.sip.staffmanagementsystem.webservice.entities.EmployeeResign;

import lombok.Data;

@Data
public class EmployeeResignDto {

	private int id;
	private EmployeeDto employee;
	private Date resignDate;
	private String remark;
	
	public static EmployeeResignDto employeeResignToEmpResignDto(EmployeeResign resign) {
		EmployeeResignDto employeeResignDto = new EmployeeResignDto();
		employeeResignDto.setId(resign.getId());
		employeeResignDto.setEmployee(EmployeeDto.employeeToEmployeeDto(resign.getEmployee(), true));
		employeeResignDto.setResignDate(resign.getResignDate());
		employeeResignDto.setRemark(resign.getRemark());
		return employeeResignDto;
	}
	
}
