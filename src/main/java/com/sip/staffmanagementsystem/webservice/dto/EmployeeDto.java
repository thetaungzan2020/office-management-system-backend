package com.sip.staffmanagementsystem.webservice.dto;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import com.sip.staffmanagementsystem.webservice.entities.Designation;
import com.sip.staffmanagementsystem.webservice.entities.Employee;
import com.sip.staffmanagementsystem.webservice.entities.type.GenderType;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class EmployeeDto {
	
	private int id;
	private String employeeCode;
	private GenderType genderType;
	private String name;
	private Designation designation;
	private String idNumber;
	private Date dateOfBirth;
	private Date actualDateOfBirth;
	private Date jointDate;
	private Date contractStartDate;
	private Date contractEndDate;
	private boolean isResign;
	private String contactNo;
	private String bankAccount;
	private String address;
	private String emergencyContactNo;
	private String email;
	private String profileName;
	private List<RepairNoteDto> repairedNotesDtos = new ArrayList<RepairNoteDto>();
	private List<EmployeeAssetDto> employeeAssetsDtos = new ArrayList<EmployeeAssetDto>();
	
	public EmployeeDto () {}
	public EmployeeDto (int id, String employeeCode, GenderType genderType, String name,Designation designation, String idNumber, Date dateOfBirth, 
						Date actualDateOfBirth, Date jointDate, Date contractStartDate, Date contaractEndDate, boolean isResign, String contactNo, 
						String bankAccount, String address, String emergencyContactNo, String email, String profileName) {
		this.id = id;
		this.employeeCode = employeeCode;
		this.genderType = genderType;
		this.name = name;
		this.designation = designation;
		this.idNumber = idNumber;
		this.dateOfBirth = dateOfBirth;
		this.actualDateOfBirth = actualDateOfBirth;
		this.jointDate = jointDate;
		this.contractStartDate = contractStartDate;
		this.contractEndDate = contaractEndDate;
		this.isResign = isResign;
		this.contactNo = contactNo;
		this.bankAccount = bankAccount;
		this.address = address;
		this.emergencyContactNo = emergencyContactNo;
		this.email = email;
		this.profileName = profileName;
	}
	public static EmployeeDto employeeToEmployeeDto(Employee employee, boolean forAll) {
		
		EmployeeDto dto = new EmployeeDto();
		dto.setId(employee.getId());
		dto.setEmployeeCode(employee.getEmployeeCode());
		dto.setGenderType(employee.getGenderType());
		dto.setName(employee.getName());
		dto.setDesignation(employee.getDesignation());
		dto.setIdNumber(employee.getIdNumber());
		dto.setDateOfBirth(employee.getDateOfBirth());
		dto.setActualDateOfBirth(employee.getActualDateOfBirth());
		dto.setJointDate(employee.getJointDate());
		dto.setContractStartDate(employee.getContractStartDate());
		dto.setContractEndDate(employee.getContractEndDate());
		dto.setResign(employee.isResign());
		dto.setContactNo(employee.getContactNo());
		dto.setBankAccount(employee.getBankAccount());
		dto.setAddress(employee.getAddress());
		dto.setEmergencyContactNo(employee.getEmergencyContactNo());
		dto.setEmail(employee.getEmail());
		dto.setProfileName(employee.getProfileName());
		if(!forAll) {
			List<RepairNoteDto> repairNoteDtoList = employee.getRepairedNotes().stream()
													.map(rn -> RepairNoteDto.repairNoteToRepairNoteDto(rn, true, false))
													.collect(Collectors.toList());
			dto.setRepairedNotesDtos(repairNoteDtoList);
			List<EmployeeAssetDto> employeeAssetDtoList = employee.getEmployeeAssets().stream()
											.map(ea -> EmployeeAssetDto.employeeAssetToEmployeeAssetDto(ea, true, false))
											.collect(Collectors.toList());
			dto.setEmployeeAssetsDtos(employeeAssetDtoList);
			
		}
		
		return dto;
	}
	
	public static Employee employeeDtoToEmployee(EmployeeDto dto) {
		
		Employee employee = new Employee();
		employee.setId(dto.getId());
		employee.setEmployeeCode(dto.getEmployeeCode());
		employee.setGenderType(dto.getGenderType());
		employee.setName(dto.getName());
		employee.setDesignation(dto.getDesignation());
		employee.setIdNumber(dto.getIdNumber());
		employee.setDateOfBirth(dto.getDateOfBirth());
		employee.setActualDateOfBirth(dto.getActualDateOfBirth());
		employee.setJointDate(dto.getJointDate());
		employee.setContractStartDate(dto.getContractEndDate());
		employee.setResign(dto.isResign());
		employee.setContactNo(dto.getContactNo());
		employee.setBankAccount(dto.getBankAccount());
		employee.setAddress(dto.getAddress());
		employee.setEmergencyContactNo(dto.getEmergencyContactNo());
//		employee.setProfile(dto.getProfile());
		employee.setEmail(dto.getEmail());
		employee.setProfileName(dto.getProfileName());
		return employee;
	}

}
