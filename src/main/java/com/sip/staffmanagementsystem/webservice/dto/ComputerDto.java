package com.sip.staffmanagementsystem.webservice.dto;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.sip.staffmanagementsystem.webservice.entities.type.AssetType;

import lombok.Data;

@Data
public class ComputerDto {

	private int id;
	private String assetName;
	private String serialNumber;
	private String computerModel;
	private String originalHardDiskSize;
	private String newHardDiskSize;
	private String memorySize;
	private String ramType;
	private String ramSpeed;
	private String osVersion;
	private String officeVersion;
	private String officeStatus;
	private String antivirusVersion;
	private String antivirusStatus;
	private boolean dvdDriveInclude;
	private String cpu;
	private Date purchaseDate;
	private float unitPrice;
	private int quantity;
	private double totalAmount;
	private String poNos;
	private String supplier;
	private String supplierPhoneNumber;
	private String remarks;
	private boolean isReturnToSip;
	private String returnRemark;
	private boolean isRemoved;
	private String removedRemark;
	private Date removedDate;
	private AssetType assetType;

	private List<RepairNoteDto> repairNotes = new ArrayList<RepairNoteDto>();
	
	
	private List<EmployeeAssetDto> employeeAssets = new ArrayList<EmployeeAssetDto>();
}
