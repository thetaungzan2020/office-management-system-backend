package com.sip.staffmanagementsystem.webservice.rest;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.sip.staffmanagementsystem.webservice.dto.AssetProfileDto;
import com.sip.staffmanagementsystem.webservice.services.AssetProfileService;

import lombok.Data;

@Controller
@Data
@RequestMapping("/api/oms/asset-profile")
public class AssetProfileController {
	
	private final AssetProfileService assetProfileService;
	
	@PostMapping(value = "/save", consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<AssetProfileDto> addAssetProfile(@RequestBody AssetProfileDto assetProfileDto){
				
		AssetProfileDto savedAssetProfileDto = assetProfileService.createAssetProfile(assetProfileDto);
		
		return ResponseEntity.ok(savedAssetProfileDto);
		
	}
	
	@PutMapping(value = "/update")
	public ResponseEntity<AssetProfileDto> updateAssetProfile(@RequestBody AssetProfileDto assetProfileDto){
		
		AssetProfileDto updatedAssetProfileDto = assetProfileService.updateAssetProfile(assetProfileDto);
		
		return ResponseEntity.ok(updatedAssetProfileDto);
	}
	
	@GetMapping(value = "/findById")
	public ResponseEntity<AssetProfileDto> findAssetProfile(@RequestParam("id") int id){
		
		AssetProfileDto foundAssetProfileDto = assetProfileService.searchAssetProfileById(id);
		
		return ResponseEntity.ok(foundAssetProfileDto);
		
	}
	
	@GetMapping(value = "/findAll")
	public ResponseEntity<List<AssetProfileDto>> findAllProfile(){
		
		List<AssetProfileDto> foundAssetProfilesDto = assetProfileService.findAllAssetProfiles();
		
		return ResponseEntity.ok(foundAssetProfilesDto);
	}
	
	@DeleteMapping(value = "/delete")
	public ResponseEntity<?> deleteAssetProfile(@RequestParam("id") int id){
		
		boolean successfullyDeleted = assetProfileService.deleteAssetProfileById(id);
		
		Map<String, Object> response = new HashMap<>();
		response.put("id", id);
		response.put("msg", successfullyDeleted ? "Successfully deleted" : "Fail To Delete");
		
		return ResponseEntity.status(successfullyDeleted ? HttpStatus.OK : HttpStatus.BAD_REQUEST).body(response);
	}

}
