package com.sip.staffmanagementsystem.webservice.rest;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.sip.staffmanagementsystem.webservice.dto.RepairNoteDto;
import com.sip.staffmanagementsystem.webservice.services.RepairNoteService;

import lombok.AllArgsConstructor;
import lombok.Data;

@Controller
@AllArgsConstructor
@Data
@RequestMapping("/api/oms/repair-note")
public class RepairNoteController {

	private final RepairNoteService noteService;
	
	@PostMapping(value = "/create")
	public ResponseEntity<?> createRepairNote(@RequestBody RepairNoteDto repairNoteDto){
		
		RepairNoteDto savedRepairNoteDto = noteService.createRepairNote(repairNoteDto);
		
		return ResponseEntity.ok(savedRepairNoteDto);
	}
	
	@PutMapping(value = "/update")
	public ResponseEntity<?> updateRepairNote(@RequestBody RepairNoteDto repairNoteDto){
		
		RepairNoteDto updatedRepairNote = noteService.updateRepairNote(repairNoteDto);
		
		return ResponseEntity.ok(updatedRepairNote);
	}
	
	@GetMapping(value = "/findById")
	public ResponseEntity<?> searchRepairNote(@RequestParam("id") int id){
		
		RepairNoteDto foundRepairNoteDto = noteService.searchRepairNote(id);
		
		return ResponseEntity.ok(foundRepairNoteDto);
	}
	
	@GetMapping(value = "/findAll")
	public ResponseEntity<?> findAllRepairNote(){
		
		List<RepairNoteDto> foundRepairNoteList = noteService.findAllRepairNote();
		
		return ResponseEntity.ok(foundRepairNoteList);
	}
	
	@DeleteMapping(value = "/delete")
	public ResponseEntity<?> deleteRepairNoe(@RequestParam("id") int id){
		
		noteService.deleteRepairNote(id);
		Map<String, Object> response = new HashMap<>();
		response.put("id", id);
		response.put("msg", "Successfully Deleted");
		
		return ResponseEntity.ok(response);
	}
	
}
