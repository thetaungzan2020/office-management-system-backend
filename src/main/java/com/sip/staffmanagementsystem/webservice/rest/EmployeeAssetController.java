package com.sip.staffmanagementsystem.webservice.rest;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.sip.staffmanagementsystem.webservice.dto.EmployeeAssetDto;
import com.sip.staffmanagementsystem.webservice.services.EmployeeAssetService;

import lombok.Data;

@Controller
@Data
@RequestMapping("/api/oms/employee-asset")
public class EmployeeAssetController {

	private final EmployeeAssetService employeeAssetService;
	
	@PostMapping(value = "/save")
	public ResponseEntity<?> createEmployeeAsset(@RequestBody EmployeeAssetDto employeeAssetDto){
		
		EmployeeAssetDto savedEmployeeAssetDto = employeeAssetService.createEmployeeAsset(employeeAssetDto);
				
		return ResponseEntity.ok(savedEmployeeAssetDto);
	}
	
	@GetMapping(value = "/findAll")
	public ResponseEntity<?> findAllEmployeeAsset(){
		
		List<EmployeeAssetDto> foundEmployeeAssets = employeeAssetService.findAllEmployeeAsset();
		
		return ResponseEntity.ok(foundEmployeeAssets);
	}
	
	@GetMapping(value = "/findById")
	public ResponseEntity<?> findEmployeeAsset(@RequestParam("id") long id){
		
		EmployeeAssetDto foundEmployeeAsset = employeeAssetService.searchEmployeeAssetById(id);
		
		return ResponseEntity.ok(foundEmployeeAsset);
	}
	
	@PutMapping(value = "/update")
	public ResponseEntity<?> updateEmoployeeAsset(@RequestBody EmployeeAssetDto employeeAssetDto){
		
		EmployeeAssetDto updatedEmployeeAssetDto = employeeAssetService.updateEmployeeAsset(employeeAssetDto);
		
		return ResponseEntity.ok(updatedEmployeeAssetDto);
	}
	
	@DeleteMapping(value = "/delete")
	public ResponseEntity<?> deleteEmployeeAsset(@RequestParam("id") long id){
		Map<String, String> response = new HashMap<String, String>();
		String msg = employeeAssetService.deleteEmployeeAsset(id);
		response.put("msg", msg);
		
		return ResponseEntity.status(HttpStatus.OK).body(response);
	}
}
