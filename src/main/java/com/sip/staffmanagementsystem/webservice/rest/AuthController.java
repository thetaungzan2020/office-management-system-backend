/*
package com.sip.staffmanagementsystem.webservice.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.sip.staffmanagementsystem.webservice.entities.User;
import com.sip.staffmanagementsystem.webservice.services.AuthService;

@RestController
@RequestMapping(value = "/api/sip")
public class AuthController {

	@Autowired
	private AuthService authService;

	@GetMapping("/auths")
	public ResponseEntity<Iterable<User>> Auths() {
		List<User> auths = authService.searchAllByAuth();

		return ResponseEntity.status(HttpStatus.FOUND).body(auths);
	}

	@PostMapping("/auths/save")
	public ResponseEntity<User> saveAuth(@RequestBody User auth) {
		System.out.println("Request Data :: " + auth);
		User createAuth = authService.createAuth(auth);
		return ResponseEntity.status(HttpStatus.CREATED).body(createAuth);
	}

	@PutMapping("/auths/search/{id}")
	public ResponseEntity<User> findAuthById(@PathVariable Integer id) {
		User responseAuth = authService.searchByAuthId(id);
		return ResponseEntity.status(HttpStatus.FOUND).body(responseAuth);
	}
}
*/
