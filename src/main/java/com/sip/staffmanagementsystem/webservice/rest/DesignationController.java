package com.sip.staffmanagementsystem.webservice.rest;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.sip.staffmanagementsystem.webservice.entities.Designation;
import com.sip.staffmanagementsystem.webservice.services.DesignationService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping(value = "/api/oms/designations")
public class DesignationController {
	
	@Autowired
	private DesignationService designationService;
	
	@GetMapping("/findAll")
	public ResponseEntity<?> findAllDesignation() {

		List<Designation> searchAllByDesignation = designationService.searchAllByDesignation();

		return ResponseEntity.ok(searchAllByDesignation);
	}

	@PostMapping("/save")
	public ResponseEntity<Designation> saveDesignation(@RequestBody Designation designation) {
		log.info("== Request Data == "+ designation);
		Designation createDesignation = designationService.createDesignation(designation);
		return ResponseEntity.status(HttpStatus.CREATED).body(createDesignation);
	}

	@PutMapping("/search")
	public ResponseEntity<Designation> findDesignationById(@RequestParam("id") Integer id) {
		Designation foundedResponse = designationService.searchByDesignationId(id);
		return ResponseEntity.status(HttpStatus.FOUND).body(foundedResponse);
	}
	
	@DeleteMapping("/delete")
	public ResponseEntity<?> deleteDesignationById(@RequestParam("id") Integer id){
		designationService.deleteByDesignationId(id);
		Map<String, Object> response = new HashMap<>();
		response.put("id", id);
		response.put("msg", "Successfully deleted");
		return ResponseEntity.ok(response);
	}

}
