/*
 * package com.sip.staffmanagementsystem.webservice.rest;
 * 
 * import java.util.HashMap; import java.util.List; import java.util.Map;
 * 
 * import org.springframework.http.ResponseEntity; import
 * org.springframework.stereotype.Controller; import
 * org.springframework.web.bind.annotation.DeleteMapping; import
 * org.springframework.web.bind.annotation.GetMapping; import
 * org.springframework.web.bind.annotation.PostMapping; import
 * org.springframework.web.bind.annotation.PutMapping; import
 * org.springframework.web.bind.annotation.RequestBody; import
 * org.springframework.web.bind.annotation.RequestMapping; import
 * org.springframework.web.bind.annotation.RequestParam;
 * 
 * import com.sip.staffmanagementsystem.webservice.dto.AssetTypeDto; import
 * com.sip.staffmanagementsystem.webservice.services.AssetTypeService;
 * 
 * import lombok.Data;
 * 
 * @Controller
 * 
 * @Data
 * 
 * @RequestMapping("/api/oms/asset-type") public class AssetTypeController {
 * 
 * private final AssetTypeService assetTypeService;
 * 
 * @PostMapping(value = "/save") public ResponseEntity<AssetTypeDto>
 * addAssetType(@RequestBody AssetTypeDto assetTypeDto){
 * 
 * AssetTypeDto savedAssetTypeDto =
 * assetTypeService.createAssetType(assetTypeDto);
 * 
 * return ResponseEntity.ok(savedAssetTypeDto); }
 * 
 * @PutMapping(value = "/update") public ResponseEntity<AssetTypeDto>
 * updateAssetType(@RequestBody AssetTypeDto assetTypeDto){
 * 
 * AssetTypeDto updatedAssetTypeDto =
 * assetTypeService.updateAsset(assetTypeDto);
 * 
 * return ResponseEntity.ok(updatedAssetTypeDto);
 * 
 * }
 * 
 * @GetMapping(value = "/findAll") public ResponseEntity<List<AssetTypeDto>>
 * findAllAssetType(){
 * 
 * List<AssetTypeDto> dtoToResponse = assetTypeService.findAllAssetTypes();
 * 
 * return ResponseEntity.ok(dtoToResponse); }
 * 
 * @GetMapping(value = "/findById") public ResponseEntity<AssetTypeDto>
 * findAssetType(@RequestParam("id") int id) {
 * 
 * AssetTypeDto dtoToResponse = assetTypeService.searchAssetTypeById(id);
 * 
 * return ResponseEntity.ok(dtoToResponse); }
 * 
 * // public ResponseEntity<?> searchAssetTypeByKeyWord(@R)
 * 
 * @DeleteMapping(value = "/delete") public ResponseEntity<?>
 * deleteAssetType(@RequestParam("id") int id) { String status =
 * assetTypeService.deleteAssetTypeById(id);
 * 
 * Map<String, Object> response = new HashMap<>(); response.put("id", id);
 * response.put("msg", status);
 * 
 * return ResponseEntity.ok(response); }
 * 
 * }
 */