package com.sip.staffmanagementsystem.webservice.rest;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.sip.staffmanagementsystem.webservice.dto.EmployeeDto;
import com.sip.staffmanagementsystem.webservice.dto.EmployeeFormDto;
import com.sip.staffmanagementsystem.webservice.services.impl.EmployeeServiceImpl;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;

@Controller
@Slf4j
@RequestMapping("/api/oms/employee")
@Data
public class EmployeeController {

	private final EmployeeServiceImpl employeeService;
	
	@PostMapping(value = "/save")
	public ResponseEntity<?> addEmployee(@RequestBody EmployeeFormDto employeeDto){
		log.info("called save api : :  :");

		EmployeeDto addedEmployeeDto = employeeService.createEmployee(employeeDto);
		
		return ResponseEntity.ok(addedEmployeeDto);
	}
	
	@GetMapping(value = "/findAll")
	public ResponseEntity<?> findAllEmployee(){
		
		List<EmployeeDto> foundEmployeesDto = employeeService.findAllEmployees();
		
		return ResponseEntity.ok(foundEmployeesDto);
	}
	
	@GetMapping(value = "/findById")
	public ResponseEntity<?> findEmployee(@RequestParam int id ){
		
		EmployeeDto foundEmployeeDto= employeeService.searchEmployeeById(id);
		
		return ResponseEntity.ok(foundEmployeeDto);
	}
	
	@PutMapping(value = "/update")
	public ResponseEntity<?> updateEmployee(@RequestBody EmployeeFormDto employeeDto){
		
		EmployeeDto updatedEmployeeDto = employeeService.updateEmployee(employeeDto);

		return ResponseEntity.ok(updatedEmployeeDto);
	}
	
	@DeleteMapping(value = "/delete")
	public ResponseEntity<?> deleteEmployee(@RequestParam int id){
		
		employeeService.deleteEmployeeById(id);
		Map<String, Object> response = new HashMap<>();
		response.put("id", id);
		response.put("msg", "Successfully Deleted");
		
		return ResponseEntity.ok(response);
	}
	
	@GetMapping(value = "/loadPhoto")
	public ResponseEntity<?> loadEmployeePhoto(@RequestParam int id){
		
		return ResponseEntity.ok(employeeService.searchPhotoByEmpId(id));
	}
	
	@GetMapping(value = "/loadCount")
	public ResponseEntity<?> loadEmployeeCounts(){
		
		return ResponseEntity.ok(employeeService.getCounts());
	}
}
