package com.sip.staffmanagementsystem.webservice.rest;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.sip.staffmanagementsystem.webservice.dto.EmployeeResignDto;
import com.sip.staffmanagementsystem.webservice.dto.EmployeeResignFormDto;
import com.sip.staffmanagementsystem.webservice.services.EmployeeResignService;

import lombok.Data;

@Controller
@Data
@RequestMapping("/api/oms/employee-resign")
public class EmployeeResignController {
	
	private final EmployeeResignService employeeResignService;
	
	@PostMapping("/save")
	public ResponseEntity<?> saveEmployeeResign(@RequestBody EmployeeResignFormDto dto){
		EmployeeResignDto savedEmployeeResign = employeeResignService.createEmployeeResign(dto);
		
		return ResponseEntity.ok(savedEmployeeResign);
	}
	
	@PutMapping("/update")
	public ResponseEntity<?> updateEmployeeResign(@RequestBody EmployeeResignFormDto dto){
		EmployeeResignDto savedEmployeeResign = employeeResignService.updateEmployeeResign(dto);
		
		return ResponseEntity.ok(savedEmployeeResign);
	}
	
	@GetMapping("/findAll")
	public ResponseEntity<?> findAllEmployeeResign(){
		List<EmployeeResignDto> employeeResigns = employeeResignService.findAllEmployeeResign();
		
		return ResponseEntity.ok(employeeResigns);
	}
	@DeleteMapping("/delete")
	public ResponseEntity<?> deleteEmployeeResign(@RequestParam("id") int id){
		employeeResignService.deleteEmployeeResignById(id);
		Map<String, String> response = new HashMap<>();
		response.put("id", ""+id);
		response.put("msg", "successfully deleted");
		return ResponseEntity.ok(response);
	}
}
