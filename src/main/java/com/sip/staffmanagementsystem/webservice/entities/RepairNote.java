package com.sip.staffmanagementsystem.webservice.entities;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import lombok.Data;

@Entity
@Data
public class RepairNote {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	private Date repairedDate;
	private int cost;
	private String remark;
	
	@ManyToOne()
	@JoinColumn(name = "asset_profile")
	private AssetProfile assetProfile;
	
	@ManyToOne()
	@JoinColumn(name = "employee")
	private Employee employee;
}
