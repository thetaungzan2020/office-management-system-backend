package com.sip.staffmanagementsystem.webservice.entities;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GenerationType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.sip.staffmanagementsystem.webservice.entities.type.GenderType;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.RequiredArgsConstructor;

@Entity
@Data
@RequiredArgsConstructor
@AllArgsConstructor
public class Employee {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	
	@Column(name = "employee_code")
	private String employeeCode;
	
	@Enumerated(EnumType.STRING)
	@Column(name = "gender_type")
	private GenderType genderType;
	private String name;
	
	@ManyToOne(fetch = FetchType.EAGER)
	private Designation designation;
	private String idNumber;
	
	@Column(name = "date_of_birth")
	private Date dateOfBirth;
	
	@Column(name = "actual_date_of_birth")
	private Date actualDateOfBirth;
	
	@Column(name = "joint_date")
	private Date jointDate;
	
	@Column(name = "contract_start_date")
	private Date contractStartDate;
	
	@Column(name = "contract_end_date")
	private Date contractEndDate;
	
	@Column(columnDefinition = "tinyint(1)", name = "is_resign")
	@JsonProperty(value = "isResign")
	private boolean isResign;
	
	@Column(name = "contact_no")
	private String contactNo;
	
	@Column(name = "bank_account")
	private String bankAccount;
	private String address;
	
	@Column(name = "emergency_contact_no")
	private String emergencyContactNo;
	
	private String email;
	@Lob
	private String profile;
	@Column(name = "profile_name")
	private String profileName;
	
	@Column(name = "created_date")
	private String createdDate;
	
	@Column(name = "updated_date")
	private String updatedDate;
	
	@OneToMany(mappedBy = "employee", fetch = FetchType.LAZY)
	@JsonIgnore
	private List<RepairNote> repairedNotes = new ArrayList<RepairNote>();
	
	@OneToMany(mappedBy = "employee", fetch = FetchType.LAZY)
	@JsonIgnore
	private List<EmployeeAsset> employeeAssets = new ArrayList<EmployeeAsset>();
	
	public void addEmployeeAsset (EmployeeAsset employeeAsset) {
		employeeAsset.setEmployee(this);
		employeeAssets.add(employeeAsset);
	}
	
	public void removeEmployeeAsset(EmployeeAsset employeeAsset) {
		employeeAsset.setEmployee(null);
		employeeAssets.remove(employeeAsset);
	}
	
	public void addRepairNote(RepairNote repairNote) {
		repairNote.setEmployee(this);
		repairedNotes.add(repairNote);
	}

	public void removeRepairNote(RepairNote repairNote) {
		repairNote.setEmployee(null);
		repairedNotes.remove(repairNote);
	}
}
