package com.sip.staffmanagementsystem.webservice.entities.type;

public enum GenderType {
	
	MALE, FEMALE, OTHER

}
