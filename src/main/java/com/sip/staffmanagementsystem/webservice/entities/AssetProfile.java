package com.sip.staffmanagementsystem.webservice.entities;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.sip.staffmanagementsystem.webservice.entities.type.AssetType;

import lombok.Data;

@Entity
@Data
public class AssetProfile {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	private String assetName;
	private String serialNumber;
	private String computerModel;
	private String originalHardDiskSize;
	private String newHardDiskSize;
	private String memorySize;
	private String ramType;
	private String ramSpeed;
	private String osVersion;
	private String officeName;
	private String officeVersion;
	private String officeStatus;
	private String antivirusVersion;
	private String antivirusStatus;
	private boolean dvdDriveInclude;
	private String cpu;
	private Date purchaseDate;
	private float unitPrice;
	private int quantity;
	private double totalAmount;
	private String poNos;
	private String supplier;
	private String supplierPhoneNumber;
	private String assignTo;
	private String coveragePeriod;
	private String remarks;
	private boolean isReturnToSip;
	private String returnRemark;
	private boolean isRemoved;
	private String removedRemark;
	private Date removedDate;
	
	@Enumerated(EnumType.STRING)
	@Column(name = "asset_type")
	private AssetType assetType;
	
	@OneToMany(mappedBy = "assetProfile", fetch = FetchType.LAZY)
	@JsonIgnore
	private List<RepairNote> repairNotes = new ArrayList<RepairNote>();
	
//	@ManyToOne(fetch = FetchType.EAGER)
//	@JoinColumn(name = "asset_type_id")
//	private AssetType assetType;
	
	@OneToMany(mappedBy = "assetProfile", fetch = FetchType.LAZY)
	@JsonIgnore
	private List<EmployeeAsset> employeeAssets = new ArrayList<EmployeeAsset>();
	
	public void addEmployeeAsset(EmployeeAsset employeeAsset) {
		employeeAsset.setAssetProfile(this);
		employeeAssets.add(employeeAsset);
	}
	
	public void removeEmployeeAsset(EmployeeAsset employeeAsset) {
		employeeAsset.setAssetProfile(null);
		employeeAssets.remove(employeeAsset);
	}
	
	public void addRepairNote(RepairNote repairNote) {
		repairNote.setAssetProfile(this);
		repairNotes.add(repairNote);
	}
	
	public void removeRepairNote(RepairNote repairNote) {
		repairNote.setAssetProfile(null);
		repairNotes.remove(repairNote);
	}
}
