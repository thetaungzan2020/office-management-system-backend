package com.sip.staffmanagementsystem.webservice.entities.type;

public enum AssetType {

	COMPUTER, EQUIPMENT, FURNITURE
}
