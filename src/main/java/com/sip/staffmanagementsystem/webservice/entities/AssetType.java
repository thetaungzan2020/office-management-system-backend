package com.sip.staffmanagementsystem.webservice.entities;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;

@Entity
@Data
public class AssetType {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	private String name;
	@Column(columnDefinition = "VARCHAR(255)")
	private String description;
	
	private LocalDateTime startedDate;
	private LocalDateTime updatedDate;
	private boolean enable;
	
	@OneToMany(mappedBy = "assetType", fetch = FetchType.LAZY)
	@JsonIgnore
	private List<AssetProfile> assetProfiles = new ArrayList<AssetProfile>();
	
	public void addAssetProfile(AssetProfile assetProfile) {
//		assetProfile.setAssetType(this);
		assetProfiles.add(assetProfile);
	}
	
	public void removeAssetProfile(AssetProfile assetProfile) {
		assetProfile.setAssetType(null);
		assetProfiles.remove(assetProfile);
	}

}
