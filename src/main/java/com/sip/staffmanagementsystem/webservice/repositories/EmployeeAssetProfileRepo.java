package com.sip.staffmanagementsystem.webservice.repositories;

import org.springframework.data.jpa.repository.support.JpaRepositoryImplementation;

import com.sip.staffmanagementsystem.webservice.entities.EmployeeAsset;

public interface EmployeeAssetProfileRepo extends JpaRepositoryImplementation<EmployeeAsset, Long>{

}
