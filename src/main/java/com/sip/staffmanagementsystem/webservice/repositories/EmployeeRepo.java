package com.sip.staffmanagementsystem.webservice.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.support.JpaRepositoryImplementation;

import com.sip.staffmanagementsystem.webservice.dto.EmployeeDto;
import com.sip.staffmanagementsystem.webservice.entities.Employee;

public interface EmployeeRepo extends JpaRepositoryImplementation<Employee, Integer>{
	
//	@Query( "SELECT new com.sip.staffmanagementsystem.webservice.dto.EmployeeDto(e.id, e.employee_code, e.genderType, e.name, e.designation,"
//			+ " e.idNumber, e.dateOfBirth, e.actualDateOfBirth, e.jointDate, e.contractDate, e.isResign, "
//			+ "e.contactNo, e.bankAccount, e.address, e.emergencyContactNo, e.email, e.profileName )"
//			+ "FROM Employee e Join e.designation d")
//	List<EmployeeDto> findAllWithoutProfile();
	
	@Query("SELECT new com.sip.staffmanagementsystem.webservice.dto.EmployeeDto(e.id, e.employeeCode, e.genderType, e.name, "
			+ "e.designation, e.idNumber, e.dateOfBirth, e.actualDateOfBirth, e.jointDate, e.contractStartDate, e.contractEndDate,"
			+ " e.isResign,  e.contactNo, e.bankAccount, e.address, e.emergencyContactNo, e.email, e.profileName) "
			+ "FROM Employee e Join e.designation d")
	List<EmployeeDto> findAllWithoutProfile();
	
	@Query("SELECT e.profile from Employee e where e.id = ?1")
	String findEmpPhotoWithId(int id);
	
	@Query("SELECT count(e.id) from Employee e where e.isResign = false")
	int getCurrentEmployeeCount();
	
	@Query("SELECT count(e.id) from Employee e where e.isResign = true")
	int getResignEmployeeCount();
	
	@Query("SELECT createdDate from Employee e where e.id = ?1")
	String getCreatedDate(int id);
}
