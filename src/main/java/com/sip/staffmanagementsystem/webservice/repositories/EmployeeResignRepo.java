package com.sip.staffmanagementsystem.webservice.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.sip.staffmanagementsystem.webservice.entities.EmployeeResign;

public interface EmployeeResignRepo extends JpaRepository<EmployeeResign, Integer>{

}
