package com.sip.staffmanagementsystem.webservice.repositories;

import org.springframework.data.jpa.repository.support.JpaRepositoryImplementation;

import com.sip.staffmanagementsystem.webservice.entities.AssetType;

public interface AssetTypeRepo extends JpaRepositoryImplementation<AssetType, Integer>{

}
