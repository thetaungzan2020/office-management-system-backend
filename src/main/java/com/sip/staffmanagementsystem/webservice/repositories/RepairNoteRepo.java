package com.sip.staffmanagementsystem.webservice.repositories;

import org.springframework.data.jpa.repository.support.JpaRepositoryImplementation;

import com.sip.staffmanagementsystem.webservice.entities.RepairNote;

public interface RepairNoteRepo extends JpaRepositoryImplementation<RepairNote, Integer>{

}
