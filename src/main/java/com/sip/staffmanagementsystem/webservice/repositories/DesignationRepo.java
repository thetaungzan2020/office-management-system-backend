package com.sip.staffmanagementsystem.webservice.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.sip.staffmanagementsystem.webservice.entities.Designation;

@Repository
public interface DesignationRepo extends JpaRepository<Designation,Integer> {

}
