package com.sip.staffmanagementsystem.webservice.repositories;

import org.springframework.data.jpa.repository.support.JpaRepositoryImplementation;

import com.sip.staffmanagementsystem.webservice.entities.AssetProfile;

public interface AssetProfileRepo extends JpaRepositoryImplementation<AssetProfile, Integer>{

}
