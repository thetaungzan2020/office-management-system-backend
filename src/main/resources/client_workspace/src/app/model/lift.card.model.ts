import { Employee } from "./employee.model";

export class LiftCard{
    id: number;
    cardNo: string;
    cardStatus: string;
    currentOwner: Employee | null;
    previousOwner: Employee | null;
    createdDate: Date;
}