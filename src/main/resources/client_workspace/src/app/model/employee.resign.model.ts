import { DatePipe } from "@angular/common";
import { Employee, EmployeeForUi } from "./employee.model";
import { Designation } from "./designation.model";

export class EmployeeResign{
    id: number;
    employee: Employee;
    resignDate: Date;
    remark: string;
}

export class EmployeeResignForUi{
    id: number;
    employee: EmployeeForUi;
    resignDate: string;
    remark: string;

    constructor(empRes: EmployeeResign, datePipe: DatePipe){
        this.id = empRes.id;
        this.employee = new EmployeeForUi(empRes.employee, datePipe);
        this.resignDate = datePipe.transform(empRes.resignDate, 'MM/dd/yyyy') as string;
        this.remark = empRes.remark;
    }

}

export class EmployeeResignForForm{
    id:number;
    employeeCode: string;
    genderType: string;
    name: string;
    designation: Designation;
    idNumber: string;
    dateOfBirth: string;
    actualDateOfBirth: string;
    jointDate: string;
    contractDate: string;
    resign: boolean;
    contactNo: string;
    bankAccount: string;
    address: string;
    emergencyContactNo: string;
    email: string;
    profile: string;
    profileName: string;
    resignDate: Date;
    remark: string;
}