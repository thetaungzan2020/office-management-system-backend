import { DatePipe } from "@angular/common";
import { Designation } from "./designation.model";

export class Employee{
    id:number;
    employeeCode: string;
    genderType: string;
    name: string;
    designation: Designation;
    idNumber: string;
    dateOfBirth: Date;
    actualDateOfBirth: Date;
    jointDate: Date;
    contractDate: Date;
    resign: boolean;
    contactNo: string;
    bankAccount: string;
    address: string;
    emergencyContactNo: string;
    email: string;
    profile: any;
    profileName: string;
}

export class EmployeeForUi{
    id:number;
    employeeCode: string;
    genderType: string;
    name: string;
    designation: Designation;
    idNumber: string;
    dateOfBirth: string;
    actualDateOfBirth: string;
    jointDate: string;
    contractDate: string;
    resign: boolean;
    contactNo: string;
    bankAccount: string;
    address: string;
    emergencyContactNo: string;
    email: string;
    // profile: any;
    profileName: string;

    constructor(employee: Employee, datePipe: DatePipe){
        this.id = employee.id;
        this.employeeCode =  employee.employeeCode;
        this.genderType = employee.genderType;
        this.name = employee.name;
        this.designation = employee.designation;
        this.idNumber = employee.idNumber;
        this.dateOfBirth = datePipe.transform(employee.dateOfBirth, 'MM/dd/yyyy') as string;
        this.actualDateOfBirth = datePipe.transform(employee.actualDateOfBirth, 'MM/dd/yyyy') as string;
        this.jointDate = datePipe.transform(employee.jointDate, 'MM/dd/yyyy') as string;
        this.contractDate = datePipe.transform(employee.contractDate, 'MM/dd/yyyy') as string;
        this.resign = employee.resign;
        this.contactNo = employee.contactNo;
        this.bankAccount = employee.bankAccount;
        this.address = employee.address;
        this.emergencyContactNo = employee.emergencyContactNo;
        this.email = employee.email;
    }
}