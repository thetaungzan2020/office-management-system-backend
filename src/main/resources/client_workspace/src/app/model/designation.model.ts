export class Designation{
    designationId: number;
    position: string;
    rankRange: number;
}