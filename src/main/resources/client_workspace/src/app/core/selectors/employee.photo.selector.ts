import { createSelector } from "@ngrx/store";
import { IAppState } from "../reducers/app.reducer";
import { EmployeePhotoInfoState } from "../reducers/employee.photo.reducer";

export const employeePhotoInfoState = (state : IAppState) => state.employeePhotoInfo;

export const selectEmployeePhotoById = (id: number) => createSelector(employeePhotoInfoState, (state: EmployeePhotoInfoState) => {
    return state.employeePhotos.find(empPhoto => empPhoto.id === id);
})