import { createSelector } from "@ngrx/store";
import { IAppState } from "../reducers/app.reducer";
import { DesignationInfoState } from "../reducers/designation.reducer";

export const selectDesignationInfoState = (state: IAppState) => state.designationInfo;

export const selectDesignationInfo = createSelector(selectDesignationInfoState, (state: DesignationInfoState) => state.designations);