import { createSelector } from "@ngrx/store";
import { IAppState } from "../reducers/app.reducer";
import { EmployeeInfoState } from "../reducers/employee.reducer";
import { state } from "@angular/animations";


export const selectEmployeeInfoState = (state: IAppState) => state.employeeInfo;

export const selectEmployees = createSelector(selectEmployeeInfoState, (state: EmployeeInfoState) => state.employees);

export const selectEmployeeById = (id: number ) =>  createSelector(selectEmployeeInfoState, (state: EmployeeInfoState) =>{
    return state.employees.find((emp) => emp.id === id);
});

export const selectEmployeeResigns = createSelector(selectEmployeeInfoState, (state: EmployeeInfoState) => state.employeeResigns);

export const selectEmployeeResignById = (id: number) => createSelector(selectEmployeeInfoState, (state: EmployeeInfoState) => {
    return state.employeeResigns.find(empRes => empRes.id === id);
});

export const selectContractEmployeeForMonth = createSelector(
    selectEmployeeInfoState,
    (state: EmployeeInfoState) => {
      const currentDate = new Date();
      const tempContractEmployees = [...state.employees].filter(emp => {
        if (emp.contractDate instanceof Date) {
          const contractMonth = emp.contractDate.getMonth();
          const contractYear = emp.contractDate.getFullYear();
          const currentMonth = currentDate.getMonth();
          const currentYear = currentDate.getFullYear();
  
          return contractMonth === currentMonth && contractYear === currentYear && !emp.resign;
        }
        return false; 
      });
  
      return tempContractEmployees;
    }
  );

export const selectEmployeeCounts = createSelector(selectEmployeeInfoState, (state: EmployeeInfoState) => state.count);