import { Injectable } from "@angular/core";
import { EmployeeService } from "../../services/employee.service";
import { Actions, createEffect, ofType } from "@ngrx/effects";
import { employeePhotoFail, loadEmployeePhoto, loadEmployeePhotoSuccess } from "../actions/employee.photo.action";
import { catchError, map, of, switchMap } from "rxjs";

@Injectable()
export class EmployeePhotoEffect{

    constructor(private employeeService: EmployeeService, private action$: Actions){}

    loadEmpPhoto$ = createEffect(() => this.action$.pipe(
        ofType(loadEmployeePhoto),
        switchMap(({id}) => this.employeeService.loadEmployeePhotoFromBE(id).pipe(
            map((loadedEmpPhoto) => loadEmployeePhotoSuccess({empPhoto: loadedEmpPhoto}))
        )),
        catchError((err) => {
            return of(employeePhotoFail({error: err}));
        })
    ))
}