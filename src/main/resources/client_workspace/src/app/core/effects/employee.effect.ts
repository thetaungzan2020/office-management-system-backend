import { Injectable } from "@angular/core";
import { Store } from "@ngrx/store";
import { IAppState } from "../reducers/app.reducer";
import { EmployeeService } from "../../services/employee.service";
import { Actions, createEffect, ofType } from "@ngrx/effects";
import { addEmployee, addEmployeeResign, addEmployeeResignSuccess, addEmployeeSuccess, employeeFail, loadEmployeeCounts, loadEmployeeCountsSuccess, loadEmployeeResign, loadEmployeeResignSuccess, loadEmployees, loadEmployeesSuccess,removeEmployee,removeEmployeeSuccess,updateEmployee,updateEmployeeSuccess } from "../actions/employee.action";
import { catchError, map, mergeMap, of, switchMap, tap } from "rxjs";
import {selectEmployees} from "../selectors/employee.selector";
import { removeEmployeePhoto } from "../actions/employee.photo.action";

@Injectable()
export class EmployeeEffect{

    constructor(private action$: Actions,
                private store: Store<IAppState>,
                private employeeService: EmployeeService){}

    // addEmployee$ = createEffect(() => this.action$.pipe(
    //     ofType(addEmployee),
    //     tap(action => console.log('addEmployee action:', action)),
    //     switchMap(({employee}) => this.employeeService.saveEmployeeToBE(employee)
    //         .pipe(
    //         map(() => loadEmployees()),
    //     )),
    //     catchError((err) => {
    //         return of(addEmployeeFail({error: err}));
    //     })
    // ));

    addEmployee$ = createEffect(() => this.action$.pipe(
        ofType(addEmployee),
        tap(action => console.log('addEmployee action:', action)),
        switchMap(({employee}) => this.employeeService.saveEmployeeToBE(employee)
            .pipe(
            map((addedEmp) => addEmployeeSuccess({employee: addedEmp})),
        )),
        catchError((err) => {
            return of(employeeFail({error: err}));
        })
    ));

    loadEmployee$ = createEffect(() => this.action$.pipe(
        ofType(loadEmployees), 
        mergeMap(() => this.employeeService.loadEmployeeFromBE()
            .pipe(
                map(employees => loadEmployeesSuccess({employees: employees}))
            )), 
            catchError((err) => {
                return of(employeeFail({error: err}));
            })
    )
    );

    updateEmployee$ = createEffect(() =>
        this.action$.pipe(
          ofType(updateEmployee),
          switchMap(({ employee }) => this.employeeService.updateEmployeeToBE(employee).pipe(
              map((updatedEmp) => {
                this.store.dispatch(removeEmployeePhoto({id: updatedEmp.id}));
                return updateEmployeeSuccess({employee: updatedEmp});
              })
            )
          ),
          catchError((err) => {return of(employeeFail({error: err}))})
        )
    );

    addEmpoyeeResign$ = createEffect(() => 
        this.action$.pipe(
            ofType(addEmployeeResign),
            switchMap(({empRes}) => this.employeeService.saveEmployeeResignToBE(empRes).pipe(
                map((savedEmpRes) => {
                    if(savedEmpRes.employee.resign){
                    this.store.dispatch(updateEmployeeSuccess({employee: savedEmpRes.employee}));
                    return addEmployeeResignSuccess({empRes: savedEmpRes});
                    }
                    return updateEmployeeSuccess({employee: savedEmpRes.employee});
                })
            )),
            catchError((err) => {return of(employeeFail({error: err}))})
        )
    );

    loadEmployeeResign$ = createEffect(() => 
        this.action$.pipe(
            ofType(loadEmployeeResign), 
            mergeMap(() => this.employeeService.loadEmployeeResignFromBE().pipe(
                map(empResigns => loadEmployeeResignSuccess({empResigns: empResigns}))
            )),
            catchError((err) => {return of(employeeFail({error: err}))})
        )
    );

    removeEmployee$ = createEffect(() => 
        this.action$.pipe(
            ofType(removeEmployee),
            switchMap(({id}) => this.employeeService.removeEmployee(id).pipe(
                map((data:{id: number, msg: string}) => {
                    this.store.dispatch(removeEmployeePhoto({id: data.id}));
                    return removeEmployeeSuccess({id: data.id})
                })
            )),
            catchError((err) => {return of(employeeFail({error: err}))})
        )
    );

    loadEmployeeCount$ = createEffect(() => 
        this.action$.pipe(
            ofType(loadEmployeeCounts),
            mergeMap(() => this.employeeService.loadEmployeeCount().pipe(
                map((data: {currentEmp: number, resignEmp: number}) => 
                    loadEmployeeCountsSuccess({data: data})
            ))),
            catchError((err) => {
                return of(employeeFail({error: err}));
            })
        )
    );
}
