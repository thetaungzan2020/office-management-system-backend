import { Injectable } from '@angular/core';
import { Actions, ofType, createEffect } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { of } from 'rxjs';
import { catchError, map, switchMap } from 'rxjs/operators';
import { IAppState } from '../reducers/app.reducer';
import { AssetService } from '../../services/asset.service';
import { addLiftCard, addLiftCardSuccess, failLiftCard, loadLiftCards, loadLiftCardsSuccess, updateLiftCard, updateLiftCardSuccess } from '../actions/lift.card.action';

@Injectable()
export class AssetEffect{

    constructor(private store: Store<IAppState>, 
                private action$: Actions,
                private assetService: AssetService){}

    saveLiftCard$ = createEffect(() => this.action$.pipe(
        ofType(addLiftCard),
        switchMap(({card}) => this.assetService.saveLiftCard(card).pipe(
            map((addedCard) => addLiftCardSuccess({card: addedCard})),
        )),
        catchError((err) => {
            return of(failLiftCard({err: err}));
        })
    ))

    updateLiftCard$ = createEffect(() => this.action$.pipe(
        ofType(updateLiftCard),
        switchMap(({card}) => this.assetService.updateLiftCard(card).pipe(
            map((updatedCard) => updateLiftCardSuccess({card: updatedCard})),
        )),
        catchError((err) => {
            return of(failLiftCard({err: err}));
        })
    ))

    loadLiftCards$ = createEffect(() => this.action$.pipe(
        ofType(loadLiftCards),
        switchMap(() => this.assetService.loadLiftCards().pipe(
            map((cards) => loadLiftCardsSuccess({cards: cards})),
        )),
        catchError((err) => {
            return of(failLiftCard({err: err}));
        })
    ))
    
}
