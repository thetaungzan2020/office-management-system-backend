import { Actions, createEffect, ofType } from "@ngrx/effects";
import { DesignationService } from "../../services/designation.service";
import { addDesignation, addDesignationSuccess, designationFail, loadDesignation, loadDesignationSuccess } from "../actions/designation.action";
import { catchError, concatMap, map, mergeMap, of, switchMap } from "rxjs";
import { Injectable } from "@angular/core";

@Injectable()
export class DesignationEffect{
    constructor(private action$: Actions, 
                private designationService: DesignationService){}

    // addDesignation$ = createEffect(() => this.action$.pipe(
    //     ofType(addDesignation),
    //     switchMap(({designation}) => this.designationService.saveDesignationToBE(designation)
    //                 .pipe(map(() => loadDesignation()))
    //     ),
    //     catchError(err => {
    //         return of(designationFail({error: err}))
    //     })
    // ))

    addDesignation$ = createEffect(() => this.action$.pipe(
        ofType(addDesignation),
        concatMap(({designation}) => this.designationService.saveDesignationToBE(designation)
                    .pipe(map((addedDes) => addDesignationSuccess({designation: addedDes})))
        ),
        catchError(err => {
            return of(designationFail({error: err}))
        })
    ))
    loadDesignation$ = createEffect(() => this.action$.pipe(
        ofType(loadDesignation),
        mergeMap(() => this.designationService.loadDesignationFromBE()
            .pipe(
                map(designations => loadDesignationSuccess({designations: designations}))
            )
        ),
        catchError(err => {
            return of(designationFail({error: err}))
        })
    ))
}