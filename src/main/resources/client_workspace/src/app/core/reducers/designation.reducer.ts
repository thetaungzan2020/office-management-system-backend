import { createReducer, on } from "@ngrx/store";
import { Designation } from "../../model/designation.model";
import { addDesignation, addDesignationSuccess, designationFail, loadDesignationSuccess } from "../actions/designation.action";

export interface DesignationInfoState{
    designations: Designation [],
    error: any
}

export const initialState: DesignationInfoState = {
    designations: [],
    error: null
}

export const designationReducer = createReducer(initialState, 
    on(loadDesignationSuccess, (state, action) => ({...state, designations: action.designations})),
    on(addDesignationSuccess, (state, action) => ({...state, designations: [...state.designations, action.designation]})),
    on(designationFail, (state, action) => ({...state, error: action.error}))
)