import { createReducer, on } from "@ngrx/store";
import { LiftCard } from "../../model/lift.card.model";
import { addLiftCardSuccess, loadLiftCardsSuccess, updateLiftCardSuccess } from "../actions/lift.card.action";

export interface LiftCardInfoState{
    liftCards : LiftCard[],
    error: any
}

const initialState: LiftCardInfoState = {
    liftCards : [],
    error: null
}

export const liftCardReducer = createReducer(initialState,
    on(addLiftCardSuccess, (state, action) => ({...state, liftCards: [...state.liftCards, action.card]})),
    on(updateLiftCardSuccess, (state, action) => {
        const tempLiftCards: LiftCard[] = [...state.liftCards];
        const index = tempLiftCards.findIndex(card => card.id === action.card.id);
        if(index !== -1){
            tempLiftCards[index] = action.card
        }
        return {...state, liftCards : tempLiftCards}
    }),
    on(loadLiftCardsSuccess, (state, action) => ({...state, liftCards: action.cards}))
    )