import { ActionReducer, ActionReducerMap, MetaReducer } from "@ngrx/store";
import { EmployeeInfoState, employeeReducer } from "./employee.reducer";
import { LocalStorageConfig, localStorageSync } from "ngrx-store-localstorage";
import { DesignationInfoState, designationReducer } from "./designation.reducer";
import { EmployeePhotoInfoState, employeePhotoReducer } from "./employee.photo.reducer";
import { AssetInfoState, assetReducer } from "./asset.reducer";

export interface IAppState{
    employeeInfo: EmployeeInfoState,
    designationInfo: DesignationInfoState,
    employeePhotoInfo: EmployeePhotoInfoState,
    assetInfo: AssetInfoState,
}

export const appReducers: ActionReducerMap<IAppState> = {
    employeeInfo: employeeReducer,
    designationInfo: designationReducer,
    employeePhotoInfo: employeePhotoReducer,
    assetInfo: assetReducer
}

const config: LocalStorageConfig = {
    rehydrate: true,
    storage: localStorage, 
    removeOnUndefined: false,
    keys: ['employeeInfo', 'designationInfo', 'assetInfo']
}

export const storageSyncReducer: MetaReducer = 
(reducer: ActionReducer<any, any> ) => localStorageSync({...config, storage: sessionStorage})(reducer);

export const metaReducers: MetaReducer<any, any> [] = [storageSyncReducer]; 