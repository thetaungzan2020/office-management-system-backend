import { createReducer, on } from "@ngrx/store";
import { EmployeePhoto } from "../../model/employee.photo.model";
import { employeePhotoFail, loadEmployeePhotoSuccess, removeEmployeePhoto } from "../actions/employee.photo.action";

export interface EmployeePhotoInfoState{
    employeePhotos: EmployeePhoto [],
    error: any
}

export const initialState: EmployeePhotoInfoState = {
    employeePhotos: [],
    error: null
}

export const employeePhotoReducer = createReducer(initialState, 
    on(loadEmployeePhotoSuccess, (state, action) => ({...state, employeePhotos : [...state.employeePhotos, action.empPhoto]})),
    on(removeEmployeePhoto, (state, action) =>{
        const tempEmpPhotos = [...state.employeePhotos].filter(empP => empP.id !== action.id);
        return {...state, employeePhotos: tempEmpPhotos};
    }),
    on(employeePhotoFail, (state, action) => ({...state, error: action.error}))
)