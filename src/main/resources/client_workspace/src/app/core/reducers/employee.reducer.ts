import { createReducer, on} from "@ngrx/store";
import { Employee } from "../../model/employee.model";
import { addEmployeeResignSuccess, addEmployeeSuccess, employeeFail, loadEmployeeCountsSuccess, loadEmployeeResignSuccess, loadEmployeesSuccess,removeEmployeeSuccess,updateEmployeeSuccess, updateEmpoyeeResignSuccess } from "../actions/employee.action";
import { EmployeeResign } from "../../model/employee.resign.model";

export interface EmployeeInfoState{
    employees: Employee[],
    employeeResigns: EmployeeResign[],
    count: {currentEmp: number, resignEmp: number},
    error: any
}

export const initialState: EmployeeInfoState ={
    employees: [],
    employeeResigns: [],
    count: {currentEmp: 0, resignEmp: 0} ,
    error: null
}

export const employeeReducer = createReducer(initialState,
    on(loadEmployeesSuccess, (state, action) => {
        let currentEmp = action.employees.filter(e => !e.resign).length;
        let resignEmp = action.employees.length - currentEmp;
        return {...state, employees: action.employees, count: {currentEmp: currentEmp, resignEmp: resignEmp}};
    }),
    on(addEmployeeSuccess, (state, action) => ({...state, employees: [...state.employees, action.employee],
        count: {currentEmp: state.count.currentEmp + 1 , resignEmp: state.count.resignEmp}})),
    on(updateEmployeeSuccess, (state, action) => {
        const tempEmployees: Employee[] = [...state.employees];
        const index = tempEmployees.findIndex(emp => emp.id === action.employee.id);

        if (index !== -1) {
            tempEmployees[index] = action.employee;
        }

        return {...state, employees: tempEmployees};
    }),
    on(removeEmployeeSuccess, (state, action) => {
        const tempEmployees = [...state.employees].filter(emp => emp.id !== action.id);
        return {...state, employees: tempEmployees, count: {currentEmp: state.count.currentEmp - 1 , resignEmp: state.count.resignEmp}};
    }),
    on(addEmployeeResignSuccess, (state, action) => ({...state, employeeResigns: [...state.employeeResigns, action.empRes], count: {currentEmp : state.count.currentEmp - 1, resignEmp: state.count.resignEmp + 1}})),
    on(updateEmpoyeeResignSuccess, (state, action) => {
        const tempEmpResigns = [...state.employeeResigns];
        const index = tempEmpResigns.findIndex(empRes => empRes.id === action.empRes.id);
        if(index !== -1){
            tempEmpResigns[index] = action.empRes;
        }
        return {...state, employeeResigns: tempEmpResigns}
    }),
    on(loadEmployeeCountsSuccess, (state, action) => ({...state, count: {currentEmp: action.data.currentEmp, resignEmp: action.data.resignEmp}})),
    on(loadEmployeeResignSuccess, (state, action) => ({...state, employeeResigns: action.empResigns})),
    on(employeeFail, (state, action) => ({...state, error: action.error}))
)
