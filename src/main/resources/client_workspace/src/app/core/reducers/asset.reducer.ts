import { combineReducers } from "@ngrx/store";
import { LiftCardInfoState, liftCardReducer } from "./lift.reducer";


export interface AssetInfoState {
    liftInfoState: LiftCardInfoState
}

export const assetReducer = combineReducers({
    liftInfoState : liftCardReducer
})

