import { createAction, props } from "@ngrx/store";
import { Designation } from "../../model/designation.model";


export const addDesignation = createAction(
    '[Designation component] add designation', props<{designation: Designation}>()
)

export const addDesignationSuccess = createAction(
    '[Designation Component] successfully added designation', props<{designation: Designation}>()
)

export const loadDesignation = createAction(
    '[Designation component] load designation'
)

export const loadDesignationSuccess = createAction(
    '[Designation Component] successfully loaded designation ', props<{designations: Designation[]}>()
)

export const designationFail = createAction(
    '[Designation Component] error on designation', props<{error: any}>() 
)