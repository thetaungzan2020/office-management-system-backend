import { createAction, props } from "@ngrx/store";
import { Employee } from "../../model/employee.model";
import { EmployeeResign, EmployeeResignForForm } from "../../model/employee.resign.model";

export const loadEmployeesSuccess = createAction(
    '[Employee Component] successfully loaded employees', props<{employees: Employee[]}>()
);

export const loadEmployees = createAction(
    '[Employee Component] load employee'
);

export const addEmployee = createAction(
    '[Employee Component] add employee', props<{employee: Employee}>()
);

export const addEmployeeSuccess = createAction(
    '[Employee Component] successfully added employee', props<{employee: Employee}>()
);

export const updateEmployee = createAction(
    '[Employee Component] update employee', props<{employee: Employee}>()
);

export const updateEmployeeSuccess = createAction(
    '[Employee Component] successfully updated employee', props<{employee: Employee}>()
)

export const removeEmployee = createAction(
    '[Employee Component] remove empoyee', props<{id: number}>()
);

export const removeEmployeeSuccess = createAction(
    '[Employee Component] successfully removed employee', props<{id: number}>()
);

export const addEmployeeResign = createAction(
    '[Employee Component] add employee resign', props<{empRes: EmployeeResignForForm}>()
);

export const addEmployeeResignSuccess = createAction(
    '[Employee Component] Successfully added employee resign', props<{empRes: EmployeeResign}>()
);

export const updateEmpoyeeResign = createAction(
    '[Employee Component] update employee resign', props<{empRes: EmployeeResign}>()
);

export const updateEmpoyeeResignSuccess = createAction(
    '[Employee Component] Successfully updated employee resign', props<{empRes: EmployeeResign}>()
);

export const loadEmployeeCounts = createAction(
    '[Employee Component] load employee counts'
);

export const loadEmployeeCountsSuccess = createAction(
    '[Employee Component] successfully loaded employee counts', props<{data: {currentEmp: number, resignEmp: number}}>()
);

export const loadEmployeeResign = createAction(
    '[Employee Component] load employee resign'
);

export const loadEmployeeResignSuccess = createAction(
    '[Employee Component] Successfully loaded employee resign', props<{empResigns: EmployeeResign[]}>()
);

export const employeeFail = createAction(
    '[Employee Component] failed to add employee', props<{error: any}>()
);
