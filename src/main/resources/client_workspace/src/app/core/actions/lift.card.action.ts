import { createAction, props } from "@ngrx/store";
import { LiftCard } from "../../model/lift.card.model";

export const addLiftCard = createAction('[Lift Card] add lift card', props<{card: LiftCard}>());

export const addLiftCardSuccess = createAction('[Lift Card] successfully added lift card',props<{card: LiftCard}>());

export const loadLiftCards = createAction('[Lift Card] load lift cards');

export const loadLiftCardsSuccess = createAction('[Lift Card] successfully loaded lift cards', props<{cards: LiftCard[]}>());

export const updateLiftCard = createAction('[Lift Card] update lift card', props<{card: LiftCard}>());

export const updateLiftCardSuccess = createAction('[Lift Card] successfully updated lift card', props<{card: LiftCard}>());

export const failLiftCard = createAction('[Lift Card] fail lift card', props<{err: any}>());