import { createAction, props } from "@ngrx/store";
import { EmployeePhoto } from "../../model/employee.photo.model";

export const loadEmployeePhoto = createAction('[Employee Component] load employee photo', props<{id: number}>());
export const loadEmployeePhotoSuccess = createAction('[Employee Component] successfully loaded photo', props<{empPhoto: EmployeePhoto}>());
export const removeEmployeePhoto = createAction('[Employee Component] remove photo', props<{id: number}>());
export const employeePhotoFail = createAction('[Employee Component] error in employee photo', props<{error: any}>());