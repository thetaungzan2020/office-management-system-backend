import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-custom-date-form',
  templateUrl: './custom-date-form.component.html',
  styleUrls: ['./custom-date-form.component.scss']
})
export class CustomDateFormComponent {

  @Input()
  formG?: any;
  
  @Input()
  formC?: any;

  @Input()
  label?: any;
  
  @Input()
  maxDate?: any = null;

  @Input()
  minDate?: any = null;
}
