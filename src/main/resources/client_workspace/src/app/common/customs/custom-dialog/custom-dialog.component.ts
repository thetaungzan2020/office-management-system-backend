import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'app-custom-dialog',
  templateUrl: './custom-dialog.component.html',
  styleUrls: ['./custom-dialog.component.scss']
})
export class CustomDialogComponent {
  @Input()
  label?: any;

  @Output()
  submitAction = new EventEmitter<boolean>(); 

  submit(){
    this.submitAction.emit(true);
  }
}
