import { LiveAnnouncer } from '@angular/cdk/a11y';
import { ChangeDetectionStrategy, Component, Input, OnChanges, OnInit, SimpleChanges, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table'; 

@Component({
  selector: 'app-custom-table',
  templateUrl: './custom-table.component.html',
  styleUrls: ['./custom-table.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CustomTableComponent implements OnInit, OnChanges {

  @Input() title?: any;

  @Input() role?: any;

  @Input() link?: any;

  @Input() searchable?: any;

  @Input() dataArray?: any;

  @Input() labelNColArray?: any;

  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  searchValue: string = '';
  dataSource!: any;
  displayedColumns: string[] = [];

  constructor(private _liveAnnouncer: LiveAnnouncer) { }

  ngOnInit(): void {
    this.dataSource = new MatTableDataSource(this.dataArray);
    this.getDisplayCol();
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (!changes['dataArray'].firstChange && changes['dataArray']) {
      this.dataSource.data = this.dataArray;
    }
  }

  getDisplayCol() {
    this.labelNColArray.forEach((data: any, i: number) => {
      this.displayedColumns[i] = data.col;
    })
  }

  changeSorting(sortState: any) {
    if (sortState.direction) {
      this.dataSource.data = this.dataArray.sort((a: any, b: any) => {
        const aValue = this.getPropertyValue(a, sortState.active);
        const bValue = this.getPropertyValue(b, sortState.active);
  
        return (sortState.direction === 'asc') ? aValue.localeCompare(bValue) : bValue.localeCompare(aValue);
      });
      this._liveAnnouncer.announce(`Sorted ${sortState.direction}ending`);
    } else {
      this._liveAnnouncer.announce('Sorting cleared');
    }
  }
  
  getPropertyValue(obj: any, propPath: string): any {
    const props = propPath.split('.');
    let value = obj;
    for (const prop of props) {
      value = value[prop];
    }
    return String(value).toLowerCase();
  }
  

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value.toLowerCase().trim();
  
    this.dataSource.data = this.dataArray.filter((data: any) => {
      return this.doesObjectContainValue(data, filterValue);
    });
  }
  
  doesObjectContainValue(obj: any, filterValue: string): boolean {
    for (const key in obj) {
      if (Object.prototype.hasOwnProperty.call(obj, key)) {
        const value = obj[key];
  
        if (typeof value === 'object' && value !== null) {
          if (this.doesObjectContainValue(value, filterValue)) {
            return true; 
          }
        } else if (typeof value === 'string' && value.toLowerCase().includes(filterValue)) {
          return true; 
        }
      }
    }
    return false;
  }
  
  
}
