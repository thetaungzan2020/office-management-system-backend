import { Component, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-errorp-tip',
  templateUrl: './errorp-tip.component.html',
  styleUrls: ['./errorp-tip.component.scss']
})
export class ErrorpTipComponent {
  @Input() formG !: FormGroup;
  @Input() formCN !: string;
  @Input() fLabel?: string;

  errorMessages: any = {
    required: 'Please enter {{fLabel}}!',
    email: 'Invalid Email format!',
    duplicate: '{{value}} is already exist!',
    minlength: 'Length of {{fLabel}} must be {{requiredLength}}',
    maxlength: 'Length of {{fLabel}} must be {{requiredLength}}',
    customIdNoPattern: 'Invalid identification number format!',
    matDatepickerMin: 'Please enter {{minDate}} or above',
    matDatepickerMax: 'Please enter {{maxDate}} or under',
    under18: 'Staff must be over 18!'
  };


  private interpolateMessage(message: string, errors: any): string {
    return message.replace(/\{\{(.*?)\}\}/g, (_, key) => {
      const value = key.split('.').reduce((acc: any, part: any) => acc && acc[part], errors);
      return value !== undefined ? value : '';
    });
  }

  getErrorMessage(errorKey: string): string {
    const control = this.formG.get(this.formCN);
    if (!control || !control.errors) return '';

    const error = control.errors[errorKey];
    if (!error) return '';

    let message = this.errorMessages[errorKey];
    if (!message) return '';

    message = message.replace('{{fLabel}}', this.fLabel);
    message = message.replace('{{value}}', control.value);
    message = message.replace('{{requiredLength}}', error.requiredLength || '');

    if (error.min) {
      message = message.replace('{{minDate}}', error.min.toISOString().split("T")[0]);
    }
    if (error.max) {
      message = message.replace('{{maxDate}}', error.max.toISOString().split("T")[0]);
    }

    return message;
  }

  getFormErrors(): string[] {
    const control = this.formG.get(this.formCN);
    return (control && control.touched)? Object.keys(control.errors || {}) : [];
  }
}