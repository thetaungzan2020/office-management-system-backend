import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-custom-card',
  templateUrl: './custom-card.component.html',
  styleUrls: ['./custom-card.component.scss']
})
export class CustomCardComponent {

  @Input()
  link?: any;
  
  @Input()
  title?: any;

  @Input()
  subtitle?: any;

  @Input()
  count?: any;

  @Input()
  date?: any;

  @Input()
  icon?: any;

  @Input()
  customStyleClass?: any;
}

