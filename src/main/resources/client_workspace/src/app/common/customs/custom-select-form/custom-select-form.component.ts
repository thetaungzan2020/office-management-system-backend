import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-custom-select-form',
  templateUrl: './custom-select-form.component.html',
  styleUrls: ['./custom-select-form.component.scss']
})
export class CustomSelectFormComponent {

  @Input()
  formG?: any;
  
  @Input()
  formC?: any;

  @Input()
  label?: any;
  
  @Input()
  datas?: any;
}
