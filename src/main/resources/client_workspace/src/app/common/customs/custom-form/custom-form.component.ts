import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'app-custom-form',
  templateUrl: './custom-form.component.html',
  styleUrls: ['./custom-form.component.scss']
})
export class CustomFormComponent {
  @Input()
  formG?: any;

  @Input()
  formC?: any;
  
  @Input()
  label?: string;

  @Input()
  dClass?: any;
  
  @Input()
  upperCase?: any = false;

  @Input()
  numberOnly?: any = false;

  @Output() 
  blurEvent = new EventEmitter<string>();

  onBlur() {
    this.blurEvent.emit(this.formC);
  }
}
