import { Directive, HostListener, Input } from "@angular/core";

@Directive({
    selector: '[toUpperCase]',
    host: {
        '(input)': '$event'
    }
})

export class UpperCaseTransformerDirective {
    @Input()
    toUpperCase: boolean = false;

    @HostListener('input', ['$event']) onInput(event: KeyboardEvent) {
        if (this.toUpperCase) {
            const input = event.target as HTMLInputElement;
            input.value = input.value.toUpperCase();
        }
    }
}