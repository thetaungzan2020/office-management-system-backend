import { Directive, HostListener, Input } from "@angular/core";
import { NgControl } from "@angular/forms";


@Directive({
    selector: '[OnlyNumber]'
})

export class NumberOnly {

    constructor(private control: NgControl) { }
    @Input() OnlyNumber: boolean;

    @HostListener('keydown', ['$event']) onKeyDown(event: any) {
        if (this.OnlyNumber) {
            let e = <KeyboardEvent>event;

            if ([46, 8, 9, 27, 13, 110, 190].indexOf(e.keyCode) !== -1 ||
                (e.keyCode === 65 && (e.ctrlKey || e.metaKey)) ||
                (e.keyCode === 67 && (e.ctrlKey || e.metaKey)) ||
                (e.keyCode === 86 && (e.ctrlKey || e.metaKey)) ||
                (e.keyCode === 88 && (e.ctrlKey || e.metaKey)) ||
                (e.keyCode >= 35 && e.keyCode <= 39)) {
                return;
            }
            if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                e.preventDefault();
            }
        }
        return;
    }

    @HostListener('input', ['$event']) onInputChange(event: any) {
        if (this.OnlyNumber) {
            let initialValue = this.control.value;
            if (this.control.value) {
                const format = /[!@#$%^&*()_+\-=\[\]{};':"\\|,\s.<>\/?a-zA-Z]+/g;
                initialValue = initialValue.replace(format, '');
            }
            this.control.control?.patchValue(initialValue);
        }
    }

}