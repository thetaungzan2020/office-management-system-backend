export function checkForDuplicateEntryInArray(dataArray: any[], fieldName: string, formGroup: any, exceptionId: any): void {
    const isDuplicate = dataArray.some((entry: any) => {
        if (entry[fieldName] === formGroup.get(fieldName)?.value && ((exceptionId != null) ? exceptionId !== entry.id : true )) {
            return true;
        } else {
            return false;
        }
    });
    
    if (isDuplicate) {
        formGroup.get(fieldName)?.setErrors({ 'duplicate': true });
    }
}

export function markAllControlsAsTouched(formGroup: any): void {
    Object.values(formGroup.controls).forEach((control: any) => {
        control.markAsTouched();
    });
}

export function cusArraySortWithArray(labelColumnArr: any, stringArray: any){
    function customSort(a: string, b: string): number {
        const indexA = labelColumnArr.findIndex((config : any) => config.col === a);
        const indexB = labelColumnArr.findIndex((config : any) => config.col === b);

        if (indexA !== -1 && indexB !== -1) {
            return indexA - indexB;
        }
            else if (indexA !== -1) {
                return -1;
            } else if (indexB !== -1) {
                return 1;
            }
            else {
                return 0;
            }
        }

        return  stringArray.sort(customSort);
}
