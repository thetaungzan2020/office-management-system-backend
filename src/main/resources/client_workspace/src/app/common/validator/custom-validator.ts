import { AbstractControl, ValidationErrors, ValidatorFn } from "@angular/forms";

export function customRequireValidator(): ValidatorFn{

    return (control:AbstractControl) : ValidationErrors | null => {
        
        return null;
    }
}

export function customIdNumberPatternValidator(): ValidatorFn {
  return (control: AbstractControl): ValidationErrors | null => {
    const pattern = /^([0-1][0-9])\/([a-zA-Z]{6})\([a-zA-Z]\)[0-9]{6}$/;

    if (control.value && !pattern.test(control.value)) {
      return { customIdNoPattern: true };
    }

    return null;
  };
}

export function customDOBValidator(): ValidatorFn{
  return (control: AbstractControl): ValidationErrors | null => {
    
    const timeDifference = new Date().getTime() - new Date(control.value).getTime();
    const millisecondsInYear = 1000 * 60 * 60 * 24 * 365.25;
    const years = timeDifference / millisecondsInYear;
    if (years < 18 ) {
      return { under18 : true };
    }

    return null;
    }
};