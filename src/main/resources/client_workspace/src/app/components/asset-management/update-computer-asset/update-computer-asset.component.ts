import { Component, TemplateRef, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';

@Component({
  selector: 'app-update-computer-asset',
  templateUrl: './update-computer-asset.component.html',
  styleUrls: ['./update-computer-asset.component.scss']
})
export class UpdateComputerAssetComponent {
  username_selected = 'user2';
  ram_selected = '8GB';
  ram_type_selected = 'DDR3L';
  os_selected = 'window10pro';
  msoffice_selected = 'none';
  antivirus_status_selected = 'true';

  constructor(private dialog: MatDialog) {}

  @ViewChild('deleteDialog') deleteDialog: TemplateRef<any>;

  showDeleteAlert() {
    this.dialog.open(this.deleteDialog);
  }
}
