import { LiveAnnouncer } from '@angular/cdk/a11y';
import { Component, TemplateRef, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort, Sort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';

@Component({
  selector: 'app-computer-list',
  templateUrl: './computer-list.component.html',
  styleUrls: ['./computer-list.component.scss']
})
export class ComputerListComponent {
  displayedComputers: string[] = [
    'no', 'details', 'computer_name', 'user_name', 'serial_num','computer_model', 'old_hd_size', 'new_hd_size', 'ram', 'ram_speed', 'ram_type', 'os_version', 'cpu', 'ms_office', 'ms_office_status', 'ms_office_version', 'antivirus_name', 'antivirus_status', 'purchase_date', 'unit_price', 'po_nos', 'rom', 'supplier_phone', 'extra_phone', 'previous_user', 'repaired_date', 'remarks' ];
 
  dataSource = new MatTableDataSource<PeriodicElement>(ELEMENT_DATA);

  constructor(private _liveAnnouncer: LiveAnnouncer, private dialog: MatDialog) {}

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild('detailsBox') detailsBox: TemplateRef<any>;

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  showDetails() {
    this.dialog.open(this.detailsBox);
  }

  announceSortChange(sortState: Sort) {
    if (sortState.direction) {
      this._liveAnnouncer.announce(`Sorted ${sortState.direction}ending`);
    } else {
      this._liveAnnouncer.announce('Sorting cleared');
    }
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  computerDetails: ComputerDetails[] = [
    { label: 'Computer Name', data: 'SIP-NB-0040'},
    { label: 'User Name', data: 'Common Area (office)'},
    { label: 'Serial Number', data: 'PF-OOWK1L 14/03'},
    { label: 'Computer Model', data: 'Lenovo T440'},
    { label: 'Old Hardisk Size', data: 'HDD 1TB'},
    { label: 'New Hardisk Size', data: 'SSD 512G'},
    { label: 'RAM', data: '8 GB'},
    { label: 'RAM Speed', data: '1600 MHz'},
    { label: 'RAM Type', data: 'DDR3'},
    { label: 'OS Version', data: 'Windows 10 Pro'},
    { label: 'CPU', data: 'Core™ i7 - 4500U CPU @ 1.80 GHz'},
    { label: 'Microsoft Office', data: '-'},
    { label: 'Microsoft Office Status', data: 'Non-Activate'},
    { label: 'Microsoft Office Version', data: '-'},
    { label: 'Antivirus Name', data: 'Trend Micro'},
    { label: 'Antivirus Status', data: 'Activate'},
    { label: 'Purchase Date', data: '23/05/2014'},
    { label: 'Unit Price', data: '$1,816.00'},
    { label: 'PO Nos.', data: 'PO-0514-0629'},
    { label: 'ROM', data: ''},
    { label: 'Supplier Phone Number', data: '123456789'},
    { label: 'Extra Phone Number', data: '987654321'},
    { label: 'Previous User', data: 'Han Lin Htun'},
    { label: 'Repaired Date', data: '13/11/2021'},
    { label: 'Remarks', data: 'changed HD SSD 512, battery changed, over heat, performance slow, touchpad error, space bar error, screen error with color shade'},
  ];
}

export interface ComputerDetails {
  label: string;
  data: string;
}

export interface PeriodicElement {
  no: number;
  details: string;
  computer_name: string;
  user_name: string;
  serial_num: string;
  computer_model: string;
  old_hd_size: string;
  new_hd_size: string;
  ram: string;
  ram_speed: string;
  ram_type: string;
  os_version: string;
  cpu: string;
  ms_office: string;
  ms_office_status: string;
  ms_office_version: string;
  antivirus_name: string;
  antivirus_status: string;
  purchase_date: string;
  unit_price: string;
  po_nos: string;
  rom: string;
  supplier_phone: number;
  extra_phone: number;
  previous_user: string;
  repaired_date: string;
  remarks: string;
}

const ELEMENT_DATA: PeriodicElement[] = [
  { no: 1, details: '', computer_name: 'SIP-NB-0040', user_name: 'Common Area (office)', serial_num: 'PF-OOWK1L 14/03', computer_model: 'Lenovo T440', old_hd_size: 'HDD 1TB', new_hd_size: 'SSD 512G', ram: '8 GB', ram_speed: '1600 MHz', ram_type: 'DDR3', os_version: 'Window 10 Pro', cpu: 'Core™ i7 - 4500U CPU @ 1.80 GHz', ms_office: 'None', ms_office_status: 'Non-Activate', ms_office_version: '-', antivirus_name: 'Trend Micro',  antivirus_status: 'Activate', purchase_date: '23/05/2014', unit_price: '$1,816.00', po_nos: 'PO-0514-0629', rom: '', supplier_phone: 9123456789, extra_phone: 9765432180, previous_user: 'Han Lin Htun', repaired_date: '13/11/2021', remarks: 'changed HD SSD 512, battery changed, over heat, performance slow, touchpad error, space bar error, screen error with color shade'},
  { no: 2, details: '', computer_name: 'SIP-NB-0040', user_name: 'Common Area (office)', serial_num: 'PF-OOWK1L 14/03', computer_model: 'Lenovo T440', old_hd_size: 'HDD 1TB', new_hd_size: 'SSD 512G', ram: '8 GB', ram_speed: '1600 MHz', ram_type: 'DDR3', os_version: 'Window 10 Pro', cpu: 'Core™ i7 - 4500U CPU @ 1.80 GHz', ms_office: 'None', ms_office_status: 'Non-Activate', ms_office_version: '-', antivirus_name: 'Trend Micro',  antivirus_status: 'Activate', purchase_date: '23/05/2014', unit_price: '$1,816.00', po_nos: 'PO-0514-0629', rom: '', supplier_phone: 9123456789, extra_phone: 9765432180, previous_user: 'Han Lin Htun', repaired_date: '13/11/2021', remarks: 'changed HD SSD 512, battery changed, over heat, performance slow, touchpad error, space bar error, screen error with color shade'}
];