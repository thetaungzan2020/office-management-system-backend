import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateLiftCardComponent } from './update-lift-card.component';

describe('UpdateLiftCardComponent', () => {
  let component: UpdateLiftCardComponent;
  let fixture: ComponentFixture<UpdateLiftCardComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [UpdateLiftCardComponent]
    });
    fixture = TestBed.createComponent(UpdateLiftCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
