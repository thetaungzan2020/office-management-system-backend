import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateLiftCardComponent } from './create-lift-card.component';

describe('CreateLiftCardComponent', () => {
  let component: CreateLiftCardComponent;
  let fixture: ComponentFixture<CreateLiftCardComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [CreateLiftCardComponent]
    });
    fixture = TestBed.createComponent(CreateLiftCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
