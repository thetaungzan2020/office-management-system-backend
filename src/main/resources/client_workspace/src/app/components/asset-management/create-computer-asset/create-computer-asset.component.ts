import { Component } from '@angular/core';

@Component({
  selector: 'app-create-computer-asset',
  templateUrl: './create-computer-asset.component.html',
  styleUrls: ['./create-computer-asset.component.scss']
})
export class CreateComputerAssetComponent {
  msoffice_selected = 'none';
}
