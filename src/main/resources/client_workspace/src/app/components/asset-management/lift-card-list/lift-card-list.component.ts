import { Component, OnInit, ViewChild } from '@angular/core';
import { Store } from '@ngrx/store';
import { IAppState } from '../../../core/reducers/app.reducer';

@Component({
  selector: 'app-lift-card-list',
  templateUrl: './lift-card-list.component.html',
  styleUrls: ['./lift-card-list.component.scss']
})
export class LiftCardListComponent implements OnInit {
  // displayedComputers: string[] = ['no', 'lift_card_no', 'lift_card_status', 'current_owner', 'previous_owner','card_created_date'];
  labelColumnConfigurations = [{label: 'No.', col: 'id'},
                               {label: 'Lift Card Number', col: 'lift_card_no'},
                               {label: 'Lift Card Status', col: 'lift_card_status'},
                               {label: 'Current Owner', col: 'current_owner'},
                               {label: 'Previous Owner', col: 'previous_owner'},
                               {label: 'Created Date', col: 'card_created_date'}]
  dataArray!: any;
  constructor(private store: Store<IAppState>) {}

  ngOnInit(): void {
    this.dataArray = ELEMENT_DATA;
  }

}

export interface PeriodicElement {
  id: number;
  lift_card_no: string;
  lift_card_status: string;
  current_owner: string;
  previous_owner: string;
  card_created_date: string;
}

const ELEMENT_DATA: PeriodicElement[] = [
  {id: 1, lift_card_no: 'SIP-Lf-01', lift_card_status: 'Even', current_owner: 'User 1', previous_owner: '-', card_created_date: '04/08/2023'},
  {id: 2, lift_card_no: 'SIP-Lf-02', lift_card_status: 'Even', current_owner: 'User 3', previous_owner: '-', card_created_date: '04/08/2023'},
  {id: 3, lift_card_no: 'SIP-Lf-03', lift_card_status: 'Even/Odd', current_owner: 'User 2', previous_owner: 'User 1', card_created_date: '04/08/2023'}
];