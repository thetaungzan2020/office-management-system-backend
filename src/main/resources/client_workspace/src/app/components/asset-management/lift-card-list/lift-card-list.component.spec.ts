import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LiftCardListComponent } from './lift-card-list.component';

describe('LiftCardListComponent', () => {
  let component: LiftCardListComponent;
  let fixture: ComponentFixture<LiftCardListComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [LiftCardListComponent]
    });
    fixture = TestBed.createComponent(LiftCardListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
