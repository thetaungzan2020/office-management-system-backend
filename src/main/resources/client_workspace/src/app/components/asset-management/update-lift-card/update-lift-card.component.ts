import { Component, TemplateRef, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';

@Component({
  selector: 'app-update-lift-card',
  templateUrl: './update-lift-card.component.html',
  styleUrls: ['./update-lift-card.component.scss']
})
export class UpdateLiftCardComponent {
  liftCardStatus = 'both';
  liftCardCurrentOwner = 'user1';
  liftCardPreviousOwner = 'user3';

  constructor(private dialog: MatDialog) {}

  @ViewChild('deleteDialog') deleteDialog: TemplateRef<any>;

  showDeleteAlert() {
    this.dialog.open(this.deleteDialog);
  }
}
