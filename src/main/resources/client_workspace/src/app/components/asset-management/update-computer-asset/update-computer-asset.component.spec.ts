import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateComputerAssetComponent } from './update-computer-asset.component';

describe('UpdateComputerAssetComponent', () => {
  let component: UpdateComputerAssetComponent;
  let fixture: ComponentFixture<UpdateComputerAssetComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [UpdateComputerAssetComponent]
    });
    fixture = TestBed.createComponent(UpdateComputerAssetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
