import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { IAppState } from '../../../core/reducers/app.reducer';
import { LiftCard } from '../../../model/lift.card.model';
import { addLiftCard, addLiftCardSuccess } from '../../../core/actions/lift.card.action';
import { selectEmployees } from '../../../core/selectors/employee.selector';
import { take } from 'rxjs';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { markAllControlsAsTouched } from '../../../common/functions/common.function';

@Component({
  selector: 'app-create-lift-card',
  templateUrl: './create-lift-card.component.html',
  styleUrls: ['./create-lift-card.component.scss']
})
export class CreateLiftCardComponent implements OnInit{

  cardStatuses = ['Even', 'Even/Odd'];
  allEmployees !: any;
  curEmployees !: any;
  liftCardForm !: FormGroup;

  constructor(private store: Store<IAppState>, private fb: FormBuilder){}

  ngOnInit(): void {
    this.store.select(selectEmployees).pipe(take(1)).subscribe(emp => {
      this.allEmployees = emp;
      this.curEmployees = emp.filter(e => !e.resign );
    });
    this.prepareLiftCardForm();
    // this.store.dispatch(addLiftCardSuccess({card: test}))
  }

  prepareLiftCardForm(){
    this.liftCardForm = this.fb.group({
      cardNo: new FormControl('', [Validators.required]),
      cardStatus: new FormControl('', [Validators.required]),
      currentOwner: new FormControl(null, [Validators.required]),
      previousOwner: new FormControl(null, [Validators.required]),
      createdDate: new FormControl(null, [Validators.required])
    })
  }

  saveLiftCard(){
    if(this.liftCardForm.valid){
      let tem= this.liftCardForm.value;
      console.log(tem);
      
      this.store.dispatch(addLiftCard({card: this.liftCardForm.value}));
      this.liftCardForm.reset();
    }else{
      markAllControlsAsTouched(this.liftCardForm);
    }
  }

}
