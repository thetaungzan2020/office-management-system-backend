import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateComputerAssetComponent } from './create-computer-asset.component';

describe('CreateComputerAssetComponent', () => {
  let component: CreateComputerAssetComponent;
  let fixture: ComponentFixture<CreateComputerAssetComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [CreateComputerAssetComponent]
    });
    fixture = TestBed.createComponent(CreateComputerAssetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
