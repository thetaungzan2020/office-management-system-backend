import { LiveAnnouncer } from '@angular/cdk/a11y';
import { Component, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort, Sort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.scss']
})
export class UserListComponent {
  displayedUsers: string[] = ['no', 'username', 'display_name', 'email', 'role', 'status'];
 
  dataSource = new MatTableDataSource<PeriodicElement>(ELEMENT_DATA);

  constructor(private _liveAnnouncer: LiveAnnouncer) {}

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  announceSortChange(sortState: Sort) {
    if (sortState.direction) {
      this._liveAnnouncer.announce(`Sorted ${sortState.direction}ending`);
    } else {
      this._liveAnnouncer.announce('Sorting cleared');
    }
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
}

export interface PeriodicElement {
  no: number;
  username: string;
  display_name: string;
  email: string;
  role: string;
  status: boolean;
}

const ELEMENT_DATA: PeriodicElement[] = [
  {no: 1, username: 'johnsmith', display_name: 'John Smith', email: 'johnsmith999@gmail.com', role: 'Super Admin', status: true},
  {no: 2, username: 'kelvingary', display_name: 'Kelvin Gary', email: 'kelvingary@gmail.com', role: 'Admin', status: false},
  {no: 3, username: 'danielnoah', display_name: 'Daniel Noah', email: 'danieldaniel@gmail.com', role: 'Admin', status: false},
  {no: 4, username: 'davidjason', display_name: 'David Jason', email: 'davidjason321@gmail.com', role: 'Super Admin', status: true},
  {no: 5, username: 'luisadam', display_name: 'Luis Adam', email: 'luisadammm@gmail.com', role: 'Admin', status: true}
];