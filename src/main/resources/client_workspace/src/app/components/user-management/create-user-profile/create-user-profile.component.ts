import { Component } from '@angular/core';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-create-user-profile',
  templateUrl: './create-user-profile.component.html',
  styleUrls: ['./create-user-profile.component.scss']
})
export class CreateUserProfileComponent {

  // multiselect with chips
  roleItems = [
    { label: 'Role 1', value: 'role-1' },
    { label: 'Role 2', value: 'role-2' },
    { label: 'Role 3', value: 'role-3' },
    { label: 'Role 4', value: 'role-4' },
    { label: 'Role 5', value: 'role-5' }
  ];

  roleControl = new FormControl([]);
  roleRemoved(option: string) {
    const menuStr: string[] = this.roleControl.value as string[];
    const index = menuStr.indexOf(option);
    if (index !== -1) {
      menuStr.splice(index, 1);
    }
    this.roleControl.setValue(this.roleControl.value);
  }
}
