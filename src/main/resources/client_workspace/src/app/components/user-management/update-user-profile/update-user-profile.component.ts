import { Component, TemplateRef, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';

@Component({
  selector: 'app-update-user-profile',
  templateUrl: './update-user-profile.component.html',
  styleUrls: ['./update-user-profile.component.scss']
})
export class UpdateUserProfileComponent {

  constructor(private dialog: MatDialog) {}

  @ViewChild('deleteDialog') deleteDialog: TemplateRef<any>;

  showDeleteAlert() {
    this.dialog.open(this.deleteDialog);
  }

  // multiselect with chips
  roleItems = [
    { label: 'Role 1', value: 'role-1' },
    { label: 'Role 2', value: 'role-2' },
    { label: 'Role 3', value: 'role-3' },
    { label: 'Role 4', value: 'role-4' },
    { label: 'Role 5', value: 'role-5' }
  ];

  roleControl = new FormControl([]);
  roleRemoved(option: string) {
    const menuStr: string[] = this.roleControl.value as string[];
    const index = menuStr.indexOf(option);
    if (index !== -1) {
      menuStr.splice(index, 1);
    }
    this.roleControl.setValue(this.roleControl.value);
  }
}
