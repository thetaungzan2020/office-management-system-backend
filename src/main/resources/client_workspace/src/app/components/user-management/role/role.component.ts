import {LiveAnnouncer} from '@angular/cdk/a11y';
import {Component, ViewChild, inject, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {MatPaginator} from '@angular/material/paginator';
import {MatTableDataSource} from '@angular/material/table';
import {RoleTypeEnums} from "../../../model/RoleTypeEnums";


@Component({
  selector: 'app-role',
  templateUrl: './role.component.html',
  styleUrls: ['./role.component.scss']
})

export class RoleComponent implements OnInit {
  protected readonly RoleTypeEnums = RoleTypeEnums;
  roleForm: FormGroup;
  roleTypeEnums = RoleTypeEnums;
  keys = Object.keys(this.roleTypeEnums);

  menuControl = new FormControl([], [Validators.required]);

  @ViewChild(MatPaginator) paginator: MatPaginator;

  announcer = inject(LiveAnnouncer);

  displayedColumns: string[] = ['no', 'role_name', 'description', 'role_types', 'menu_management'];

  dataSource = new MatTableDataSource<PeriodicElement>(ELEMENT_DATA);

  constructor(private _liveAnnouncer: LiveAnnouncer, private fb: FormBuilder) {
  }

  ngOnInit(): void {
    this.createdRoleForm();
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
  }

  createdRoleForm() {
    this.roleForm = this.fb.group({
      "roleName": new FormControl("", [Validators.required]),
      "description": new FormControl("", [Validators.required]),
      "roleType": new FormControl( this.keys[0], [Validators.required]),
      "menus": this.menuControl
    })
  }

  handleRoleSave() {
    this.roleForm.controls['menus'].setValue(this.menuControl.value);
    console.log(this.roleForm.value);
  }

  defaultCheckeSuperAdminRole(index: number): any {
       return index == 0;
  }

  OnCheckSuperAdmin() : any {
    switch (this.roleForm.controls['roleType'].value) {
      case "ADMIN" : {
        return true;
      }
      case "SUPERADMIN" : {
        this.roleForm.controls['menus'].setValue(new Array());
        return false;
      }
    }
  }

  // multiselect with chips
  menuItems = [
    {label: 'User Management', value: 'user-management'},
    {label: 'Staff Management', value: 'staff-management'},
    {
      label: 'Asset Management',
      children: [
        {label: 'Computer List', value: 'computer-list'},
        {label: 'Office Equipment', value: 'office-equipment'},
        {label: 'Office Furniture', value: 'office-furniture'},
      ],
    },
    {
      label: 'Document Management',
      children: [
        {label: 'Appraisal', value: 'appraisal'},
        {label: 'PettyCash & Expense', value: 'pettycash-expense'},
        {label: 'Staff Contract', value: 'staff-contract'},
        {label: 'Home Contract', value: 'home-contract'},
        {label: 'Voucher', value: 'voucher'},
      ],
    },
  ];


  menuRemoved(option: string) {
    const menuStr: string[] = this.menuControl.value as string[];
    const index = menuStr.indexOf(option);
    if (index !== -1) {
      menuStr.splice(index, 1);
    }
    this.menuControl.setValue(this.menuControl.value);
  }


}

export interface PeriodicElement {
  no: number;
  role_name: string;
  description: string;
  role_types: string;
  menu_management: string;
}

const ELEMENT_DATA: PeriodicElement[] = [
  {
    no: 1,
    role_name: 'Staff Management Role',
    description: 'allow all access of staff information management',
    role_types: 'Super Admin',
    menu_management: 'Staff Management, User Management'
  },
  {
    no: 2,
    role_name: 'Asset Management Role',
    description: 'allow all access of asset information management',
    role_types: 'Admin',
    menu_management: 'Asset Management'
  },
  {
    no: 2,
    role_name: 'Document Management Role',
    description: 'allow all access of document information management',
    role_types: 'Admin',
    menu_management: 'Document Management'
  }
];


