import { Component, ElementRef, OnInit, Optional } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { LoginRequestDto } from '../../model/login-request-dto';
import { LoginService } from '../../services/login.service';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  requiredString: string = "This field is required.";
  hide = true;

  request?: LoginRequestDto;
  loginForm: FormGroup<any>;
  loginValidate: boolean | undefined;


  constructor(private loginService: LoginService, private fb: FormBuilder, private router: Router, private el: ElementRef) {

    this.loginForm = fb.group(
      {
        username: ['', Validators.required],
        password: ['', Validators.required]
      }
    )
  }

  ngOnInit(): void {

  }

  /*handleLogin() {
    if (this.loginForm.valid) {
      new LoginRequestDto();
      console.log("Request : " + loginRequest);
    }
  }*/


  handleLogin() {

    if (this.loginForm.invalid) {
      this.loginForm.controls['username'].markAsTouched();
      this.loginForm.controls['password'].markAsTouched();
      this.el.nativeElement.scrollIntoView({ behavior: "smooth", block: "start", inline: 'nearest' });
    }
    else {
      this.request = new LoginRequestDto(this.loginForm.value.username, this.loginForm.value.password);
      console.log(`Request Data : `, this.request);
      this.loginService.saveLogin(this.request).subscribe(response => {
        console.log("Response Data : ", response);
        this.router.navigate(['/main']);
      }, error => {
        console.log(error.error);
      })
    }





























    /* if (this.loginForm.valid) {
       this.request = new LoginRequestDto(this.loginForm.value.username, this.loginForm.value.password);
       console.log(this.request);
       this.loginService.saveLogin(this.request).subscribe(data=> {
         console.log(data);
       });
     }*/
    // console.log(this.request);
    // this.loginService.saveLogin(this.request).subscribe(data => {
    //   console.log(data);

    // },error=> {
    //   alert(error.error)
    // });

    // this.router.navigate(['/main']);




    // if (this.loginForm.invalid) {
    //   this.el.nativeElement.scrollIntoView({ behavior: "smooth", block: "start", inline: 'nearest' });
    //   // this.el.nativeElement.focus();
    // } else {
    //   this.loginService.saveLogin(this.request).subscribe((response:any) => {
    //     this.router.navigate(['/main']);
    //   })
    // }
  }

}




