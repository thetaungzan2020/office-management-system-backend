import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { IAppState } from '../../core/reducers/app.reducer';
import { selectEmployeeCounts } from '../../core/selectors/employee.selector';
import { loadEmployeeCounts } from '../../core/actions/employee.action';
import { take } from 'rxjs';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit{
  displayedColumns: string[] = ['position', 'name', 'weight', 'symbol'];
  currentEmployeeCount!: number;
  constructor(private store:Store<IAppState>){}

  ngOnInit(): void {
    this.store.select(selectEmployeeCounts).pipe(take(2)).subscribe(data =>{
      if(data.currentEmp == 0){
        this.store.dispatch(loadEmployeeCounts());
      }
      this.currentEmployeeCount = data.currentEmp;
    });
  }
}
