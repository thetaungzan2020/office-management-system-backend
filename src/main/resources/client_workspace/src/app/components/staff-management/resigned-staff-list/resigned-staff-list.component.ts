import { LiveAnnouncer } from '@angular/cdk/a11y';
import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { IAppState } from '../../../core/reducers/app.reducer';
import { selectEmployeeResigns } from '../../../core/selectors/employee.selector';
import {  EmployeeResignForUi } from '../../../model/employee.resign.model';
import { DatePipe } from '@angular/common';
import { MatTableDataSource } from '@angular/material/table';

@Component({
  selector: 'app-resigned-staff-list',
  templateUrl: './resigned-staff-list.component.html',
  styleUrls: ['./resigned-staff-list.component.scss']
})
export class ResignedStaffListComponent implements OnInit{
  // displayedColumns: string[] = ['no', 'staff_code', 'gender', 'name', 'designation', 'rank', 'email', 'id_num', 'dob', 'actual_birthday', 'bank_account', 'contact_no', 'address', 'join_date', 'contract_date'];
  displayedColumns: string[] = ['id',  'employeeCode', 'genderType','name', 'position', 'rankRange', 'email', 'idNumber', 'dateOfBirth', 'actualDateOfBirth', 'bankAccount', 'contactNo', 'address', 'jointDate', 'contractDate'];

  dataSource!: any;
  empResignsForUi!: EmployeeResignForUi[];
  
  labelColumnConfigurations: any [] =[{label: 'No.', col: 'id'},
                                      {label: 'Name', col: 'employee.name'},
                                      {label: 'Email Address', col: 'employee.email'},
                                      {label: 'Staff Code', col: 'employee.employeeCode'},
                                      {label: 'Designation', col: 'employee.designation.position'},
                                      {label: 'Rank', col: 'employee.designation.rankRange'},
                                      {label: 'Gender', col: 'employee.genderType'},
                                      {label: 'Identification Number', col: 'employee.idNumber'},
                                      {label: 'Date Of Birth', col: 'employee.dateOfBirth'},
                                      {label: 'Actual Birthday', col: 'employee.actualDateOfBirth'},
                                      {label: 'Contact Number', col: 'employee.contactNo'},
                                      {label: 'Emergency Contact Number', col: 'employee.emergencyContactNo'},
                                      {label: 'Bank Account', col: 'employee.bankAccount'},
                                      {label: 'Staff Address', col: 'employee.address'},
                                      {label: 'Joint Date', col: 'employee.jointDate'},
                                      {label: 'Contract Date', col: 'employee.contractDate'},
                                      {label: 'Resign Date', col: 'resignDate'},
                                      {label: 'Remark', col: 'remark'}];
  constructor(private _liveAnnouncer: LiveAnnouncer, private store: Store<IAppState>, private datePipe: DatePipe) {}


  ngOnInit(): void {
    this.store.select(selectEmployeeResigns).subscribe(data => {
      this.empResignsForUi = [];
      data.forEach(d => {
        let empResignForUi = new EmployeeResignForUi(d, this.datePipe);
        this.empResignsForUi.push(empResignForUi);
      })
    });
    this.dataSource = new MatTableDataSource(this.empResignsForUi);
  }

}
