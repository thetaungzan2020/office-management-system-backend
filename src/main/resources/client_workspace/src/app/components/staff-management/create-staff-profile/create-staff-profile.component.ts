import { Component, ElementRef, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Store } from '@ngrx/store';
import { IAppState } from '../../../core/reducers/app.reducer';
import { addEmployee, loadEmployeeResign, loadEmployees } from '../../../core/actions/employee.action';
import { Designation } from '../../../model/designation.model';
import { selectDesignationInfo } from '../../../core/selectors/designation.selector';
import { selectEmployeeResigns, selectEmployees } from '../../../core/selectors/employee.selector';
import { loadDesignation } from '../../../core/actions/designation.action';
import { Employee } from '../../../model/employee.model';
import { customDOBValidator } from '../../../common/validator/custom-validator';
import { take } from 'rxjs';
import { checkForDuplicateEntryInArray, markAllControlsAsTouched } from '../../../common/functions/common.function';

@Component({
  selector: 'app-create-staff-profile',
  templateUrl: './create-staff-profile.component.html',
  styleUrls: ['./create-staff-profile.component.scss']
})
export class CreateStaffProfileComponent implements OnInit{

  selectedFileName: string = '';
  preview: string = 'assets/images/staff_profile_img.jpg';

  designationList!: Designation[];
  staffForm!: FormGroup;
  currentStuffCount!: number;
  resignStuffCount!: number;
  employees!: Employee [];
  expireContractCount!:number ;
  currentDate!: Date;
  maxDOB!: Date;

  genderList: any[]= ['MALE', 'FEMALE'];

  constructor(private fb: FormBuilder, private store: Store<IAppState>, private el: ElementRef){}

  ngOnInit(): void {
      this.prepareStaffForm();
      this.extractDataFromStore();
  }

  extractDataFromStore(){
    this.store.select(selectDesignationInfo).pipe(take(1)).subscribe(data => {
      this.designationList = data;
      let count = 0;
      if(data.length <= 0 && count <= 0){
        this.store.dispatch(loadDesignation());
        count++;
      }
    });
    this.store.select(selectEmployees).subscribe(data => {
      let count = 0;
      this.employees = data;
      this.currentStuffCount = data.filter(d => !d.resign).length;
      if(data.length <= 0 && count <= 0) {
        this.store.dispatch(loadEmployees());
        count++;
      }
    });
    this.store.select(selectEmployeeResigns).pipe(take(1)).subscribe(data => {
      let count = 0;
      this.resignStuffCount = data.length;
      if(data.length <= 0 && count <=0){
        this.store.dispatch(loadEmployeeResign());
        count++;
      }
    });

    // this.store.select(selectContractEmployeeForMonth).subscribe(data => {
    //   console.log("count of contract which will be full this month ", data.length);
      
    // })
  }

  prepareStaffForm(){
    this.currentDate = new Date();
    this.maxDOB = new Date(this.currentDate.getFullYear() - 18, this.currentDate.getMonth(), this.currentDate.getDate());    
    this.staffForm = this.fb.group({
      employeeCode: new FormControl('', [Validators.required]),
      genderType: new FormControl('', [Validators.required]),
      name: new FormControl('', [Validators.required]),
      email: new FormControl('', [Validators.required, Validators.email]),
      designation: new FormControl(null, [Validators.required]),
      idNumber: new FormControl('', [Validators.required, Validators.minLength(18), Validators.maxLength(18)/*, customIdNumberPatternValidator()*/]),
      dateOfBirth: new FormControl('', [Validators.required, customDOBValidator()]),
      actualDateOfBirth: new FormControl('', [Validators.required, customDOBValidator()]),
      jointDate: new FormControl('', [Validators.required]),
      contractDate: new FormControl('', [Validators.required]),
      contactNo: new FormControl('', [Validators.required]),
      bankAccount: new FormControl('', [Validators.required]),
      address: new FormControl('', [Validators.required]),
      emergencyContactNo: new FormControl('', [Validators.required])
    })
  }

  onFileSelected(event: any): void {
    const files = event.target.files;
    if (files.length > 0) {
      this.selectedFileName = files[0].name;

      const reader = new FileReader();
      reader.onload = (e: any) => {
        this.preview = e.target.result;
      };
      reader.readAsDataURL(files[0]);
    } 
    else {
      this.selectedFileName = '';
      this.preview = 'assets/images/staff_profile_img.jpg';
    }
  }

  createStaff(){
    if(this.staffForm.valid){
      
      let tempEmp = this.staffForm.value;
      tempEmp.profile = this.preview;
      tempEmp.profileName = this.selectedFileName;
      this.store.dispatch(addEmployee({employee: tempEmp}));
      this.staffForm.reset();
      this.selectedFileName = '';
      this.preview = 'assets/images/staff_profile_img.jpg';
    }else{
      console.log("called create staff and invalid form");
      
      markAllControlsAsTouched(this.staffForm);
    } 
  }

  checkDuplication(event: any){
    checkForDuplicateEntryInArray(this.employees, event, this.staffForm, null);
  }

}
