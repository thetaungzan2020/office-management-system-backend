import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ResignedStaffListComponent } from './resigned-staff-list.component';

describe('ResignedStaffListComponent', () => {
  let component: ResignedStaffListComponent;
  let fixture: ComponentFixture<ResignedStaffListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ResignedStaffListComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(ResignedStaffListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
