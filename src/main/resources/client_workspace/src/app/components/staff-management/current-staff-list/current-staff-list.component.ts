import { LiveAnnouncer } from '@angular/cdk/a11y';
import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { IAppState } from '../../../core/reducers/app.reducer';
import { selectEmployees } from '../../../core/selectors/employee.selector';
import { EmployeeForUi } from '../../../model/employee.model';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-current-staff-list',
  templateUrl: './current-staff-list.component.html',
  styleUrls: ['./current-staff-list.component.scss']
})
export class CurrentStaffListComponent implements OnInit {

  displayedColumns: string[] = ['id', 'name', 'email', 'employeeCode', 'designation.position', 'designation.rankRange',
    'genderType', 'idNumber', 'dateOfBirth', 'actualDateOfBirth', 'contactNo', 'emergencyContactNo', 'bankAccount', 'address', 'jointDate', 'contractDate'];
  labelColumnConfigurations: any[] = [{ label: 'No', col: 'id' },
  { label: 'Name', col: 'name' },
  { label: 'Email Address', col: 'email' },
  { label: 'Staff Code', col: 'employeeCode' },
  { label: 'Designation', col: 'designation.position' },
  { label: 'Rank', col: 'designation.rankRange' },
  { label: 'Gender', col: 'genderType' },
  { label: 'Identification Number', col: 'idNumber' },
  { label: 'Date of Birth', col: 'dateOfBirth' },
  { label: 'Actual Birthday', col: 'actualDateOfBirth' },
  { label: 'Contact Number', col: 'contactNo' },
  { label: 'Emergency Contact Number', col: 'emergencyContactNo' },
  { label: 'Bank Account', col: 'bankAccount' },
  { label: 'Staff Address', col: 'address' },
  { label: 'Joint Date', col: 'jointDate' },
  { label: 'Contract Date', col: 'contractDate' }];

  empForUi: EmployeeForUi[] = [];

  constructor(private store: Store<IAppState>, private datePipe: DatePipe) { }

  ngOnInit(): void {
    this.store.select(selectEmployees).subscribe(data => {
      this.empForUi = [];
      let tempEmps = data.filter(d => !d.resign);
      tempEmps.forEach(e => {
        let empUi = new EmployeeForUi(e, this.datePipe);
        this.empForUi.push(empUi);
      })
    });
  }

}
