import { Component, ElementRef, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Store } from '@ngrx/store';
import { IAppState } from '../../../core/reducers/app.reducer';
import { ActivatedRoute, Router } from '@angular/router';
import { selectEmployees } from '../../../core/selectors/employee.selector';
import { Employee } from '../../../model/employee.model';
import { Designation } from '../../../model/designation.model';
import { selectDesignationInfo } from '../../../core/selectors/designation.selector';
import { loadEmployeePhoto } from '../../../core/actions/employee.photo.action';
import { selectEmployeePhotoById } from '../../../core/selectors/employee.photo.selector';
import { MatDialog } from '@angular/material/dialog';
import { addEmployeeResign, removeEmployee, updateEmployee } from '../../../core/actions/employee.action';
import { take } from 'rxjs';
import { customDOBValidator, customIdNumberPatternValidator } from '../../../common/validator/custom-validator';
import { checkForDuplicateEntryInArray, markAllControlsAsTouched } from '../../../common/functions/common.function';

@Component({
  selector: 'app-update-staff-profile',
  templateUrl: './update-staff-profile.component.html',
  styleUrls: ['./update-staff-profile.component.scss']
})
export class UpdateStaffProfileComponent implements OnInit{
  
  selectedFileName: string = '';
  preview: string = '';

  employeeIdToEdit!: number;
  employees!: Employee[];
  staffForm!: FormGroup;
  designationList!: Designation[];

  resignDate!: Date;
  remark: string = '';
  currentDate!: Date;
  maxDOB!: Date;

  genderList: any[]= ['MALE', 'FEMALE'];
  textForDialog = "Are you sure you  want to delete ?";

  constructor(private store: Store<IAppState>, private fb: FormBuilder, private router: Router,
              private route: ActivatedRoute, private dialog: MatDialog, private el: ElementRef){}

  @ViewChild('deleteDialog') deleteDialog: TemplateRef<any>;

  ngOnInit(): void {
    this.extractDataFrmStore();
      this.prepareStuffForm();
  }

  prepareStuffForm(){
    this.currentDate = new Date();
    this.maxDOB = new Date(this.currentDate.getFullYear() - 18, this.currentDate.getMonth(), this.currentDate.getDate());
    
    let data = this.employees.find(e => e.id === this.employeeIdToEdit);
      if(data !== undefined){
        let tempDes = this.designationList.find(des => {
          if(data !== undefined){
            return des.designationId === data.designation.designationId;
          }else {
            return false;
          }
        });
        this.staffForm = this.fb.group({
          id: [data.id],
          employeeCode: [data.employeeCode, [Validators.required]],
          genderType: [data.genderType, [Validators.required]],
          name: [data.name, [Validators.required]],
          email: [data.email, [Validators.required, Validators.email]],
          designation: [tempDes, [Validators.required]],
          idNumber: [data.idNumber, [Validators.required,  Validators.minLength(18), Validators.maxLength(18), customIdNumberPatternValidator()]],
          dateOfBirth: [data.dateOfBirth, [Validators.required, customDOBValidator()]],
          actualDateOfBirth: [data.actualDateOfBirth, [Validators.required, customDOBValidator()]],
          jointDate: [data.jointDate, [Validators.required]],
          contractDate: [data.contractDate, [Validators.required]],
          contactNo: [data.contactNo, [Validators.required]],
          bankAccount: [data.bankAccount, [Validators.required]],
          address: [data.address, [Validators.required]],
          emergencyContactNo: [data.emergencyContactNo, [Validators.required]],
          isResign: [(data.resign == true) ? 'on' : 'off'], 
          resignDate: [''],
          remark: ['']
        });

        if(data.profileName === null || data.profileName === ''){
          this.selectedFileName = 'sample.jpg';
          this.preview = 'assets/images/staff_profile_img.jpg';
        }else{
          this.selectedFileName = data.profileName;
        }
      }
  }

  extractDataFrmStore(){
    this.employeeIdToEdit = parseInt(this.route.snapshot.paramMap.get('id') as string);
    this.store.select(selectDesignationInfo).subscribe(data => this.designationList = data);
    this.store.select(selectEmployees).subscribe(data => this.employees = data);
    this.store.select(selectEmployeePhotoById(this.employeeIdToEdit)).pipe(take(2)).subscribe(data => {
      if(data === undefined || data === null){
        this.store.dispatch(loadEmployeePhoto({id: this.employeeIdToEdit}));
      }
      this.preview = data?.profile;
    });
  }

  checkRequireForResign(): boolean {
    const isResignChecked = this.staffForm.get('isResign')?.value === 'on';
    const validators = isResignChecked ? [Validators.required] : null;
  
    this.staffForm.get('resignDate')?.setValidators(validators);
    this.staffForm.get('remark')?.setValidators(validators);
  
    return isResignChecked;
  }
  

  onFileSelected(event: any): void {
    const files = event.target.files;
    if (files.length > 0) {
      this.selectedFileName = files[0].name;

      const reader = new FileReader();
      reader.onload = (e: any) => {
        this.preview = e.target.result;
      };
      reader.readAsDataURL(files[0]);
    } 
    else {
      this.selectedFileName = '';
      this.preview = '';
    }
  }

  updateEmployee(){    
    if(this.staffForm.valid ){
      let tempEmp = this.staffForm.value;
        tempEmp.profileName = this.selectedFileName;
        tempEmp.profile = this.preview;
        tempEmp.resign = this.checkRequireForResign();
        // this.store.dispatch(updateEmployee({employee: this.stuffFrom.value}));
      
        if(tempEmp.resign){
          this.store.dispatch(addEmployeeResign({empRes: tempEmp}));
        }else{
          this.store.dispatch(updateEmployee({employee: tempEmp}));
        }
        this.router.navigateByUrl('/main/staff-management/staff-list');
    }else{
      markAllControlsAsTouched(this.staffForm);
    }
  }

  showDeleteAlert() {
    this.dialog.open(this.deleteDialog);
  }

  receiveSubmitAction(event: any){
    if(event === true){
      // this.deleteEmployee();
      console.log("delete was called");
      
    }
  }
  deleteEmployee(){
    this.store.dispatch(removeEmployee({id: this.employeeIdToEdit}));
    this.router.navigateByUrl('/main/staff-list');    
  }

  checkDuplication(event: any){

    checkForDuplicateEntryInArray(this.employees, event, this.staffForm, this.employeeIdToEdit);
  }

}
