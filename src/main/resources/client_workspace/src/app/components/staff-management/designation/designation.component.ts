import { ChangeDetectionStrategy, Component, ElementRef, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Store } from '@ngrx/store';
import { IAppState } from '../../../core/reducers/app.reducer';
import { addDesignation } from '../../../core/actions/designation.action';
import { selectDesignationInfo } from '../../../core/selectors/designation.selector';
import {markAllControlsAsTouched} from "../../../common/functions/common.function";

@Component({
  selector: 'app-designation',
  templateUrl: './designation.component.html',
  styleUrls: ['./designation.component.scss'],
  changeDetection: ChangeDetectionStrategy.Default
})
export class DesignationComponent implements OnInit{

  dataSource!: any;

  designationForm!: FormGroup;
  designationList!: any;
  labelColumnConfigurations : any [] = [{label:'No.', col: 'designationId'},
                                        {label:'Designation Name', col: 'position'},
                                        {label:'Rank', col: 'rankRange'},
  ]
  constructor(private fb: FormBuilder, private store: Store<IAppState>, private el: ElementRef) {}

  ngOnInit(): void {
      this.prepareDesignationForm();
      this.store.select(selectDesignationInfo).subscribe(data => {
        this.designationList = data;
      })
  }

  prepareDesignationForm(){
    this.designationForm = this.fb.group({
      position: new FormControl('', [Validators.required]),
      rankRange: new FormControl('', [Validators.required])
    })
  }

  createDesignation(){
    if(this.designationForm.valid){
    this.store.dispatch(addDesignation({designation: this.designationForm.value}));
    this.designationForm.reset();
    this.prepareDesignationForm();
    }else{
        markAllControlsAsTouched(this.designationForm);
      this.el.nativeElement.scrollIntoView({behavior: "smooth", block: "start", inline: "nearest"})
    }
  }
}

