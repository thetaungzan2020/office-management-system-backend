import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CurrentStaffListComponent } from './current-staff-list.component';

describe('CurrentStaffListComponent', () => {
  let component: CurrentStaffListComponent;
  let fixture: ComponentFixture<CurrentStaffListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [CurrentStaffListComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(CurrentStaffListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
