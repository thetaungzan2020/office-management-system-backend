import { Component, HostListener } from '@angular/core';

@Component({
  selector: 'app-main-layout',
  templateUrl: './main-layout.component.html',
  styleUrls: ['./main-layout.component.scss']
})
export class MainLayoutComponent {

  ngOnInit(): void { 
    this.onResize(window.innerWidth);
  }

  constructor() {
  }

  isOpened: boolean = true;
  desktopViewWidth: number = 1100;

  @HostListener('window:resize', ['$event.target.innerWidth'])
  onResize(width: number) {
    this.isOpened = width >= this.desktopViewWidth;
  }
}
