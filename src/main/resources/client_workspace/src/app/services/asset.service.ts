import { HttpClient } from "@angular/common/http";
import { LiftCard } from "../model/lift.card.model";
import { Observable } from "rxjs";

export class AssetService{

    baseUrl = 'http://localhost:8080/api/oms/designations';
    constructor(private http: HttpClient){}

    saveLiftCard(card: any): Observable<LiftCard>{
        return this.http.post<LiftCard>(this.baseUrl+'', card);
    }

    updateLiftCard(card: any): Observable<LiftCard>{
        return this.http.put<LiftCard>(this.baseUrl+'', card);
    }

    loadLiftCards(): Observable<LiftCard[]>{
        return this.http.get<LiftCard[]>(this.baseUrl);
    }
}