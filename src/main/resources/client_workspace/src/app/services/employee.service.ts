import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Employee } from '../model/employee.model';
import { Observable } from 'rxjs';
import { EmployeePhoto } from '../model/employee.photo.model';
import { EmployeeResign, EmployeeResignForForm } from '../model/employee.resign.model';

@Injectable({
  providedIn: 'root'
})
export class EmployeeService {

  baseUrlForEmployee = 'http://localhost:8080/api/oms/employee';
  baseUrlForEmployeeResign = 'http://localhost:8080/api/oms/employee-resign';
  constructor(private http: HttpClient) { }

  saveEmployeeToBE(employee: Employee): Observable<Employee>{
    return this.http.post<Employee>(this.baseUrlForEmployee+"/save", employee);
  }

  saveEmployeeResignToBE(empRes: EmployeeResignForForm): Observable<EmployeeResign>{
    return this.http.post<EmployeeResign>(this.baseUrlForEmployeeResign+"/save", empRes);
  }

  loadEmployeeFromBE(): Observable<Employee[]>{
    return this.http.get<Employee[]>(this.baseUrlForEmployee+"/findAll");
  }

  loadEmployeeResignFromBE(): Observable<EmployeeResign[]>{
    return this.http.get<EmployeeResign[]>(this.baseUrlForEmployeeResign+"/findAll");
  }

  updateEmployeeToBE(employee: Employee): Observable<Employee>{
      return this.http.put<Employee>(this.baseUrlForEmployee+"/update", employee);
  }

  updateEmployeeResignToBE(empRes: EmployeeResign): Observable<EmployeeResign>{
    return this.http.put<EmployeeResign>(this.baseUrlForEmployeeResign+"/update", empRes);
  }

  removeEmployee(id: number): Observable<{id: number, msg: string}>{
    return this.http.delete<{id: number, msg: string}>(`${this.baseUrlForEmployee}/delete?id=${id}`);
  } 

  loadEmployeePhotoFromBE(id: number): Observable<EmployeePhoto>{
    return this.http.get<EmployeePhoto>(`${this.baseUrlForEmployee}/loadPhoto?id=${id}`);
  }

  loadEmployeeCount(): Observable<{currentEmp: number, resignEmp: number}>{
    return this.http.get<{currentEmp: number, resignEmp: number}>(this.baseUrlForEmployee+"/loadCount");
  }
}
