import { HttpClient } from "@angular/common/http";
import { Designation } from "../model/designation.model";
import { Observable } from "rxjs";
import { Injectable } from "@angular/core";

@Injectable({
    providedIn: 'root'
})
export class DesignationService{

    baseUrl = 'http://localhost:8080/api/oms/designations'
    constructor(private http: HttpClient){}

    saveDesignationToBE(designation: Designation): Observable<Designation>{        
        return this.http.post<Designation>(this.baseUrl+"/save", designation);
    }
    
    loadDesignationFromBE(): Observable<Designation[]>{
        return this.http.get<Designation[]>(this.baseUrl+"/findAll");
    }
}