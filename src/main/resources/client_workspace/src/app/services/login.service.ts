import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { LoginRequestDto } from '../model/login-request-dto';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  loginUrl = "http://localhost:8080/api/oms/login/authenticate";
  
  constructor(private http: HttpClient) { }

  saveLogin(data: LoginRequestDto) : Observable<any> {
    return this.http.post(this.loginUrl,data);
  }
}
