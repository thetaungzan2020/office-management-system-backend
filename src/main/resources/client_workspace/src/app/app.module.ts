import { NgModule, isDevMode } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
// Material Form Controls
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatRadioModule } from '@angular/material/radio';
import { MatSelectModule } from '@angular/material/select';
import { MatSliderModule } from '@angular/material/slider';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
// Material Navigation
import { MatMenuModule } from '@angular/material/menu';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatToolbarModule } from '@angular/material/toolbar';
// Material Layout
import { MatCardModule } from '@angular/material/card';
import { MatDividerModule } from '@angular/material/divider';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatListModule } from '@angular/material/list';
import { MatStepperModule } from '@angular/material/stepper';
import { MatTabsModule } from '@angular/material/tabs';
import { MatTreeModule } from '@angular/material/tree';
// Material Buttons & Indicators
import { MatButtonModule } from '@angular/material/button';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { MatBadgeModule } from '@angular/material/badge';
import { MatChipsModule } from '@angular/material/chips';
import { MatIconModule } from '@angular/material/icon';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatRippleModule, MatNativeDateModule } from '@angular/material/core';
// Material Popups & Modals
import { MatBottomSheetModule } from '@angular/material/bottom-sheet';
import { MatDialogModule } from '@angular/material/dialog';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatTooltipModule } from '@angular/material/tooltip';
// Material Data tables
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSortModule } from '@angular/material/sort';
import { MatTableModule } from '@angular/material/table';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LoginService } from './services/login.service';
import { HttpClientModule } from '@angular/common/http';
import { MainLayoutComponent } from './components/main-layout/main-layout.component';
import { SidebarComponent } from './components/sidebar/sidebar.component';
import { CurrentStaffListComponent } from './components/staff-management/current-staff-list/current-staff-list.component';
import { ResignedStaffListComponent } from './components/staff-management/resigned-staff-list/resigned-staff-list.component';
import { CreateStaffProfileComponent } from './components/staff-management/create-staff-profile/create-staff-profile.component';
import { UpdateStaffProfileComponent } from './components/staff-management/update-staff-profile/update-staff-profile.component';
import { DesignationComponent } from './components/staff-management/designation/designation.component';
import { CreateUserProfileComponent } from './components/user-management/create-user-profile/create-user-profile.component';
import { UserListComponent } from './components/user-management/user-list/user-list.component';
import { UpdateUserProfileComponent } from './components/user-management/update-user-profile/update-user-profile.component';
import { RoleComponent } from './components/user-management/role/role.component';
import { ComputerListComponent } from './components/asset-management/computer-list/computer-list.component';
import { CreateComputerAssetComponent } from './components/asset-management/create-computer-asset/create-computer-asset.component';
import { StoreModule } from '@ngrx/store';
import { appReducers, metaReducers } from './core/reducers/app.reducer';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { LoginComponent } from './components/login/login.component';
import { EffectsModule } from '@ngrx/effects';
import { EmployeeEffect } from './core/effects/employee.effect';
import { DesignationEffect } from './core/effects/designation.effect';
import { EmployeePhotoEffect } from './core/effects/employee.photo.effect';
import { DatePipe } from '@angular/common';
import { NumberOnly } from './common/directive/onlynumber.directive';
import { UpperCaseTransformerDirective } from './common/directive/uppercase.directive';
import { UpdateComputerAssetComponent } from './components/asset-management/update-computer-asset/update-computer-asset.component';
import { CreateLiftCardComponent } from './components/asset-management/create-lift-card/create-lift-card.component';
import { LiftCardListComponent } from './components/asset-management/lift-card-list/lift-card-list.component';
import { UpdateLiftCardComponent } from './components/asset-management/update-lift-card/update-lift-card.component';
import { CustomCardComponent } from './common/customs/custom-card/custom-card.component';
import { CustomDateFormComponent } from './common/customs/custom-date-form/custom-date-form.component';
import { CustomDialogComponent } from './common/customs/custom-dialog/custom-dialog.component';
import { CustomFormComponent } from './common/customs/custom-form/custom-form.component';
import { CustomSelectFormComponent } from './common/customs/custom-select-form/custom-select-form.component';
import { CustomTableComponent } from './common/customs/custom-table/custom-table.component';
import { ErrorpTipComponent } from './common/customs/errorp.tip/errorp-tip.component';

export const effects = [
  EmployeeEffect,
  DesignationEffect,
  EmployeePhotoEffect
]

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    DashboardComponent,
    MainLayoutComponent,
    SidebarComponent,
    CurrentStaffListComponent,
    ResignedStaffListComponent,
    CreateStaffProfileComponent,
    UpdateStaffProfileComponent,
    DesignationComponent,
    CreateUserProfileComponent,
    UserListComponent,
    UpdateUserProfileComponent,
    RoleComponent,
    ComputerListComponent,
    CreateComputerAssetComponent,
    NumberOnly,
    UpperCaseTransformerDirective,
    UpdateComputerAssetComponent,
    CreateLiftCardComponent,
    LiftCardListComponent,
    UpdateLiftCardComponent,
    CustomCardComponent,
    CustomDateFormComponent,
    CustomDialogComponent,
    CustomFormComponent,
    CustomSelectFormComponent,
    CustomTableComponent,
    ErrorpTipComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    MatAutocompleteModule,
    MatCheckboxModule,
    MatDatepickerModule,
    MatNativeDateModule, 
    MatFormFieldModule,
    MatInputModule,
    MatRadioModule,
    MatSelectModule,
    MatSliderModule,
    MatSlideToggleModule,
    MatMenuModule,
    MatSidenavModule,
    MatToolbarModule,
    MatCardModule,
    MatDividerModule,
    MatExpansionModule,
    MatGridListModule,
    MatListModule,
    MatStepperModule,
    MatTabsModule,
    MatTreeModule,
    MatButtonModule,
    MatButtonToggleModule,
    MatBadgeModule,
    MatChipsModule,
    MatIconModule,
    MatProgressSpinnerModule,
    MatProgressBarModule,
    MatRippleModule,
    MatBottomSheetModule,
    MatDialogModule,
    MatSnackBarModule,
    MatTooltipModule,
    MatPaginatorModule,
    MatSortModule,
    MatTableModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
    FormsModule,
    HttpClientModule,
    StoreModule.forRoot(appReducers, {metaReducers, 
                                      runtimeChecks: {
                                        strictActionImmutability: false,
                                        strictStateImmutability: false
                                      },}),
    StoreDevtoolsModule.instrument({ maxAge: 25, logOnly: !isDevMode() }),
    EffectsModule.forRoot(effects)
  ],
  providers: [LoginService, DatePipe],
  bootstrap: [AppComponent],
  exports: [
    NumberOnly,
    UpperCaseTransformerDirective
  ]
})
export class AppModule { }

