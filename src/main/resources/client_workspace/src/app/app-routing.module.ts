import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './components/login/login.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { MainLayoutComponent } from './components/main-layout/main-layout.component';
import { CurrentStaffListComponent } from './components/staff-management/current-staff-list/current-staff-list.component';
import { CreateStaffProfileComponent } from './components/staff-management/create-staff-profile/create-staff-profile.component';
import { ResignedStaffListComponent } from './components/staff-management/resigned-staff-list/resigned-staff-list.component';
import { UpdateStaffProfileComponent } from './components/staff-management/update-staff-profile/update-staff-profile.component';
import { DesignationComponent } from './components/staff-management/designation/designation.component';
import { UserListComponent } from './components/user-management/user-list/user-list.component';
import { CreateUserProfileComponent } from './components/user-management/create-user-profile/create-user-profile.component';
import { UpdateUserProfileComponent } from './components/user-management/update-user-profile/update-user-profile.component';
import { RoleComponent } from './components/user-management/role/role.component';
import { CreateComputerAssetComponent } from './components/asset-management/create-computer-asset/create-computer-asset.component';
import { ComputerListComponent } from './components/asset-management/computer-list/computer-list.component';
import { UpdateComputerAssetComponent } from './components/asset-management/update-computer-asset/update-computer-asset.component';
import { CreateLiftCardComponent } from './components/asset-management/create-lift-card/create-lift-card.component';
import { LiftCardListComponent } from './components/asset-management/lift-card-list/lift-card-list.component';
import { UpdateLiftCardComponent } from './components/asset-management/update-lift-card/update-lift-card.component';

const routes: Routes = [
  {
    path:"",
    redirectTo:"/login",
    pathMatch:"full"
  },
  {
    path:"login",
    component:LoginComponent
  },
  {
    path:"main",
    component:MainLayoutComponent,
    children: [
      {
        path:"",
        redirectTo:"dashboard",
        pathMatch:"full"
      },
      {
        path:"dashboard",
        component:DashboardComponent
      },
      {
        path: "staff-management",
        children: [
          {
            path: "",
            component: CreateStaffProfileComponent
          },
          {
            path: "staff-list",
            component: CurrentStaffListComponent
          },
          {
            path: "resigned-staff-list",
            component: ResignedStaffListComponent
          },
          {
            path: "update-staff-profile/:id",
            component: UpdateStaffProfileComponent
          },
          {
            path: "staff-designation",
            component: DesignationComponent
          }
        ]
      }, 
      {
        path: "user-management",
        children: [
          {
            path: "",
            component: CreateUserProfileComponent
          },
          {
            path: "user-list",
            component: UserListComponent
          },
          {
            path: "update-user-profile",
            component: UpdateUserProfileComponent
          },
          {
            path: "user-role",
            component: RoleComponent
          }
        ]
      },
      {
        path: 'asset-management',
        children: [
          {
            path: 'computer-asset',
            children: [
              {
                path: "",
                component: ComputerListComponent
              },
              {
                path: 'add-computer-asset',
                component: CreateComputerAssetComponent
              },
              {
                path: 'update-computer-asset',
                component: UpdateComputerAssetComponent
              }
            ]
          },
          {
            path: 'lift-card',
            children: [
              {
                path: "",
                component: LiftCardListComponent
              },
              {
                path: 'add-lift-card',
                component: CreateLiftCardComponent
              },
              {
                path: 'update-lift-card/:id',
                component: UpdateLiftCardComponent
              }
            ]
          }
        ]
      }
    ]
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
